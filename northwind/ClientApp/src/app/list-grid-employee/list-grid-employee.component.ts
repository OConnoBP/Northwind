import { Component, OnInit, Input } from '@angular/core';
import { Employee } from '../models/employee';

@Component({
  selector: 'tr[app-list-grid-employee]',
  templateUrl: './list-grid-employee.component.html',
  styleUrls: ['./list-grid-employee.component.css']
})

export class ListGridEmployeeComponent implements OnInit {

  @Input() employee: Employee;

  constructor() { }

  ngOnInit() { }
}
