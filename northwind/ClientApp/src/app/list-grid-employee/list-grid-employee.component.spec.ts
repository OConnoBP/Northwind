import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListGridEmployeeComponent } from './list-grid-employee.component';

describe('ListGridEmployeeComponent', () => {
  let component: ListGridEmployeeComponent;
  let fixture: ComponentFixture<ListGridEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListGridEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListGridEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
