import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})

export class EmployeesComponent implements OnInit {

  private _employees: any;
  private _employees_loaded: boolean = false;
  private _error: boolean = false;

  constructor(private employee_service: EmployeeService) {
    this.getEmployees();
  }

  ngOnInit() { }

  getEmployees() {
    this.employee_service.getEmployees().subscribe(data => {
      this.handleLoadEmployeesSuccess(data);
    }, error => {
      this.handleLoadEmployeesError(error);
    });
  }

  handleLoadEmployeesSuccess(employees: any) {
    this._employees = employees;

    this._employees_loaded = true;
    this._error = false;
  }

  handleLoadEmployeesError(error: object) {
    this._error = true;
  }
}
