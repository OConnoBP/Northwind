import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SupplierService } from '../services/supplier.service';
import { Supplier } from '../models/supplier';
import { Product } from '../models/Product';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-supplier-details',
  templateUrl: './supplier-details.component.html',
  styleUrls: ['./supplier-details.component.css']
})

export class SupplierDetailsComponent implements OnInit {

  private _supplier: Supplier = undefined;
  private _supplier_loaded: boolean = false;
  private _supplier_error: boolean = false;

  private _products: Product[] = undefined;
  private _products_loaded: boolean = false;
  private _products_error: boolean = false;

  constructor(private route: ActivatedRoute, private supplier_service: SupplierService, private product_service: ProductService) {
    this.getSupplier(this.route.snapshot.params['id']);
    this.getProducts(this.route.snapshot.params['id']);
  }

  ngOnInit() { }

  getSupplier(id: string): void {
    this.supplier_service.getSuppliers().subscribe(data => {
      this.handleSupplierLoadSuccess(data);
    }, error => {
      this.handleSupplierLoadError(error);
    });
  }

  handleSupplierLoadSuccess(supplier: any): void {
    this._supplier = new Supplier(supplier);

    this._supplier_loaded = true;
    this._supplier_error = false;
  }

  handleSupplierLoadError(error: object): void {
    this._supplier_error = true;
  }

  getProducts(id: string): void {
    this.product_service.getProductsBySupplierId(id).subscribe(data => {
      this.handleProductsLoadSuccess(data);
    }, error => {
      this.handleProductsLoadError(error);
    })
  }

  handleProductsLoadSuccess(products: any): void {
    this._products = products;

    this._products_loaded = true;
    this._products_error = false;
  }

  handleProductsLoadError(error: object): void {
    this._products_error = true;
  }
}
