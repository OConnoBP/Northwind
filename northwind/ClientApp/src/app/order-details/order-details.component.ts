import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Order } from '../models/order';
import { Product } from '../models/Product';
import { OrderService } from '../services/order.service';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})

export class OrderDetailsComponent implements OnInit {

  private _order: Order = undefined;
  private _order_loaded: boolean = false;
  private _order_error: boolean = false;

  private _products: Product[] = undefined;
  private _products_loaded: boolean = false;
  private _products_error: boolean = false;

  constructor(private route: ActivatedRoute, private order_service: OrderService, private product_service: ProductService) {
    this.getOrder();
    this.getProducts();
  }

  ngOnInit() { }

  getOrder(): any {
    this.order_service.getOrderByOrderId(this.route.snapshot.params['id']).subscribe(data => {
      this.handleOrderLoadSuccess(data);
    }, error => {
      this.handleOrderLoadError(error);
    });
  }

  handleOrderLoadSuccess(order: any) {
    this._order = new Order(order);
    console.log(this._order);

    this._order_loaded = true;
    this._order_error = false;
  }

  handleOrderLoadError(error: object) {
    this._order_error = false;
  }

  getProducts(): any {
    this.product_service.getProductsByOrderId(this.route.snapshot.params['id']).subscribe(data => {
      this.handleProductsLoadSuccess(data);
    }, error => {
      this.handleProductsLoadError(error);
    })
  }

  handleProductsLoadSuccess(products: any) {
    this._products = products;

    this._products_loaded = true;
    this._products_error = false;
  }

  handleProductsLoadError(error: object) {
    this._products_error = false;
  }
}
