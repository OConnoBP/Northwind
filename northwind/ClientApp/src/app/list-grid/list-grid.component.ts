import { Component, OnInit, Input, OnChanges, SimpleChanges, ElementRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Category } from '../models/Category';
import { Product } from '../models/Product';
import { Customer } from '../models/customer';
import { Order } from '../models/order';
import { Supplier } from '../models/supplier';
import { Employee } from '../models/employee';
import { Shipper } from '../models/shipper';

@Component({
  selector: 'app-list-grid',
  templateUrl: './list-grid.component.html',
  styleUrls: ['./list-grid.component.css']
})

export class ListGridComponent implements OnInit, OnChanges {
  //  An array of data for the table
  @Input() data: any;
  //  The properties in the data that the table will show
  @Input() object_properties: any;
  //  Headers are the natural language identifiers for the object_properties
  @Input() headers: any;
  //  The route is the route that clicking on an item in the table will take the user
  @Input() route: string;
  //  The name of the table
  @Input() name: string;

  //  All of the rows in the table
  private _all_rows: any = [];
  private _data_type: string = "";
  private _data_loaded: boolean = false;
  private _current_page: number = 1;
  private _items_per_page: number = 10;
  private _header_sort_direction: any = [];
  //  The rows that are displayed on the page
  private _displayed_rows: any = [];

  constructor(private sanitizer: DomSanitizer) {
    for (let i = 0; i < this.headers; i++) {
      this._header_sort_direction.push(undefined);
    }
  }

  ngOnInit() { }

  private getNumberOfRows(): number {
    return this.data.length;
  }

  get itemsPerPage() {
    return this._items_per_page;
  }

  set itemsPerPage(items_per_page: number) {
    this._items_per_page = items_per_page;
    this.setDisplayedRows();
  }

  private setDisplayedRows(): void {
    //  Reset the list of displayed rows
    this._displayed_rows = [];

    //  Determine the min/max indicies being displayed in the grid
    let first_index_for_page = (this._current_page * this._items_per_page) - this._items_per_page;
    let last_index_for_page = this._current_page * this._items_per_page;

    for (let i = first_index_for_page; i < last_index_for_page; i++) {
      if (this.data[i] === undefined) {
        //  If there are less items than the number of items per page, we're done
        break;
      } else {
        this._displayed_rows.push(this.data[i]);
      }
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.data.currentValue !== undefined) {
      this._data_type = this.determineObjectType(this.data[0]);
      this.setDisplayedRows();
      this._data_loaded = true;
    }
  }

  private determineObjectType(an_object): string {
    if (an_object instanceof Category) {
      return 'Category';
    } else if (an_object instanceof Product) {
      return 'Product';
    } else if (an_object instanceof Customer) {
      return 'Customer';
    } else if (an_object instanceof Order) {
      return 'Order';
    } else if (an_object instanceof Supplier) {
      return 'Supplier';
    } else if (an_object instanceof Employee) {
      return 'Employee';
    } else if (an_object instanceof Shipper) {
      return 'Shipper';
    }
  }

  private onPageChange(page): void {
    this._current_page = page;
    this.setDisplayedRows();
  }

  //This is broken since objects are used now
  private sortColumn(event: any, sort_dir: string): void {
    let header_index = event.srcElement.innerText;
    let new_sort_direction = undefined;

    for (let i = this.data.length - 1; i >= 0; i--) {
      for (let j = 1; j <= i; j++) {
        if (this._header_sort_direction[header_index] === 'asc' || this._header_sort_direction[header_index] === undefined) {
          new_sort_direction = 'desc';
          if (this.data[j - 1][header_index - 1] > this.data[j][header_index - 1]) {
            let temp = this.data[j - 1];
            this.data[j - 1] = this.data[j];
            this.data[j] = temp;
          }
        } else if (this._header_sort_direction[header_index] === 'desc') {
          new_sort_direction = 'asc'
          if (this.data[j - 1][header_index - 1] < this.data[j][header_index - 1]) {
            let temp = this.data[j - 1];
            this.data[j - 1] = this.data[j];
            this.data[j] = temp;
          }
        }
      }
    }

    this._header_sort_direction[header_index] = new_sort_direction;
    //  After the columns are resorted, reset the displayed rows
    this.setDisplayedRows();
  }
}
