import { Component, OnInit } from '@angular/core';
import { ShipperService } from '../services/shipper.service';
import { Shipper } from '../models/shipper';

@Component({
  selector: 'app-shippers',
  templateUrl: './shippers.component.html',
  styleUrls: ['./shippers.component.css']
})

export class ShippersComponent implements OnInit {

  private _shippers: Shipper[];
  private _shippers_loaded: boolean = false;
  private _error: boolean = false;

  constructor(private shipper_service: ShipperService) {
    console.log('a');
    this.getShippers();
  }

  ngOnInit() { }

  getShippers() {
    this.shipper_service.getShippers().subscribe(data => {
      this.handleShippersLoadSuccess(data);
    }, error => {
      this.handleShippersLoadError(error);
    });
  }

  handleShippersLoadSuccess(shipper: any) {
    console.log(shipper);
    this._shippers = shipper;

    this._shippers_loaded = true;
    this._error = false;
  }

  handleShippersLoadError(error: object) {
    this._error = true;
  }
}
