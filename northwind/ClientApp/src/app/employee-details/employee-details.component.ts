import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Employee } from '../models/employee';
import { Order } from '../models/order';
import { EmployeeService } from '../services/employee.service';
import { OrderService } from '../services/order.service';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})

export class EmployeeDetailsComponent implements OnInit {

  private _employee: Employee = undefined;
  private _employee_loaded: boolean = false;
  private _employee_error: boolean = false;

  private _orders: Order[] = undefined;
  private _orders_loaded: boolean = false;
  private _orders_error: boolean = false;

  constructor(private route: ActivatedRoute, private employee_service: EmployeeService, private order_service: OrderService) {
    this.getEmployee();
    this.getOrders();
  }

  ngOnInit() { }

  getEmployee(): any {
    this.employee_service.getEmployeeByEmployeeId(this.route.snapshot.params['id']).subscribe(data => {
      this.handleEmployeeLoadSuccess(data);
    }, error => {
      this.handleEmployeeLoadError(error);
    });
  }

  handleEmployeeLoadSuccess(employee: any) {
    this._employee = new Employee(employee);

    this._employee_loaded = true;
    this._employee_error = false;
  }

  handleEmployeeLoadError(error: object) {
    console.log(error);
    this._employee_error = true;
  }

  getOrders(): any {
    this.order_service.getOrdersByEmployeeId(this.route.snapshot.params['id']).subscribe(data => {
      this.handleOrdersLoadSuccess(data);
    }, error => {
      this.handleOrdersLoadError(error);
    });
  }

  handleOrdersLoadSuccess(orders: any) {
    this._orders = orders;

    this._orders_loaded = true;
    this._orders_error = false;
  }

  handleOrdersLoadError(error: object) {
    this._orders_error = true;
  }
}
