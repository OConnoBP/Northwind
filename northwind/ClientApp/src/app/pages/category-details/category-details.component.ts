import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from '../../services/category.service';
import { ProductService } from '../../services/product.service';
import { Category } from '../../models/Category';
import { DomSanitizer } from '@angular/platform-browser';
import { MzToastService } from 'ngx-materialize';
import { Product } from '../../models/Product';

@Component({
  selector: 'app-category-details',
  templateUrl: './category-details.component.html',
  styleUrls: ['./category-details.component.css']
})

export class CategoryDetailsComponent implements OnInit {

  //  The category that the user is editing/viewing (dirty)
  private _category: Category;
  //  A representation of the category that is saved in the database (clean)
  private _clean_category: Category;
  //  A list of products associated with the category
  private _products: Product[] = undefined;
  //  Flag for the state of the category's load
  private _category_loaded: boolean = false;
  //  Flag for the state of the product's load
  private _products_loaded: boolean = false;
  //  Flag for if there is an error loading the category
  private _category_error: boolean = false;
  //  Flag for if there is an error loading the products
  private _products_error: boolean = false;
  //  Flag for when the category is being saved
  private _saving_category: boolean = false;

  @ViewChild('modal') modal;

  constructor(private route: ActivatedRoute, private category_service: CategoryService, private product_service: ProductService,
    private sanitizer: DomSanitizer, private toast_service: MzToastService) {
    this.loadCategory();
    this.loadProducts();
  }

  ngOnInit() {
    
  }

  private loadCategory(): void {
    this.category_service.getCategory(this.route.snapshot.params['id']).subscribe((category: Category) => {
      this.handleCategoryLoadSuccess(category);
    }, error => {
      this.handleCategoryLoadError(error);
    });
  }

  private handleCategoryLoadSuccess(category: Category): void {
    this._clean_category = new Category(category);
    this._category = new Category(category);

    this._category_error = false;
    this._category_loaded = true;
  }

  private handleCategoryLoadError(error: object): void {
    this._category_error = true;
  }

  private loadProducts(): void {
    this.product_service.getProductsByCategoryId(this.route.snapshot.params['id']).subscribe(data => {
      this.handleProductsLoadSuccess(data);
    }, error => {
      this.handleProductsLoadError(error);
    });
  }

  private handleProductsLoadSuccess(products: any): void {
    this._products = products;

    this._products_error = false;
    this._products_loaded = true;
  }

  private handleProductsLoadError(error: object): void {
    this._products_error = true;
  }

  private saveCategory(): void {
    this._saving_category = true;
    this.category_service.updateCategory(this._category.toJSON()).subscribe(data => {
      setTimeout(() => {
        this._saving_category = false;
        Object.assign(this._clean_category, this._category);

        this.modal.closeModal();
        this.toast_service.show('Category saved successfully!', 7000);
      }, 5000);
    }, error => {
      this._saving_category = false;
      this.toast_service.show('Unable to save category...', 7000);
    });
  }
  
  private asdf(event): void {
    const file = event.target.files[0];

    var reader = new FileReader();

    reader.onload = this._handleReaderLoaded.bind(this);

    reader.readAsBinaryString(file);

    console.log(file);
  }

  private _handleEditCategoryModalClose(): void {
    let clean = true;
    for(let property in this._category) {
      if (this._category[property] !== this._clean_category[property]) {
        clean = false;
        break;
      }
    }
    if (!clean) {
      Object.assign(this._category, this._clean_category);
    }
  }

  private _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this._category.picture = `data:image/bmp;base64,${btoa(binaryString)}`;
  }
}
