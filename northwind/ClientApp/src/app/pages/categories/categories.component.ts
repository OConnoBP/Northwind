import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../services/category.service';
import { Category } from '../../models/Category';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})

export class CategoriesComponent implements OnInit {

  private _categories: Category[] = undefined;
  private _categories_loaded: boolean = false;
  private _error: boolean = false;

  constructor(private category_service: CategoryService) {
    this.loadCategories();
  }

  ngOnInit() { }

  private loadCategories(): void {
    this.category_service.getCategories().subscribe(data => {
      this.handleCategoryLoadSuccess(data);
    }, error => {
      this.handleCategoryLoadError(error);
    });
  }

  private handleCategoryLoadSuccess(data: any): void {
    this._categories = data;
    this._categories_loaded = true;
    this._error = false;
  }

  private handleCategoryLoadError(error: object): void {
    this._error = true;
  }
}
