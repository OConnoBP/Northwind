import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CustomerService } from '../../services/customer.service';
import { Customer } from '../../models/customer';
import { OrderService } from '../../services/order.service';
import { Order } from '../../models/order';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.css']
})

export class CustomerDetailsComponent implements OnInit {

  private _customer: Customer = undefined;
  private _clean_customer: Customer = undefined;
  private _customer_loaded: boolean = false;
  private _customer_error: boolean = false;

  private _orders: Order[] = undefined;
  private _orders_loaded: boolean = false;
  private _orders_error: boolean = false;

  constructor(private route: ActivatedRoute, private customer_service: CustomerService, private order_service: OrderService) {
    this.loadCustomer();
    this.loadOrders();
  }

  ngOnInit() { }

  private loadCustomer() {
    this.customer_service.getCustomer(this.route.snapshot.params['id']).subscribe(data => {
      this.handleCustomerLoadSuccess(data);
    }, error => {
      this.handleCustomerLoadError(error);
    });
  }

  private handleCustomerLoadSuccess(customer: any) {
    this._customer = new Customer(customer);
    this._clean_customer = new Customer(customer);

    this._customer_error = false;
    this._customer_loaded = true;
  }

  private handleCustomerLoadError(error: any) {
    this._customer_error = true;
  }

  private loadOrders() {
    this.order_service.getOrdersByCustomerId(this.route.snapshot.params['id']).subscribe(data => {
      this.handleOrdersLoadSuccess(data);
    }, error => {
      this.handleOrdersLoadError(error);
    });
  }

  private handleOrdersLoadSuccess(orders: any) {
    this._orders = orders;

    this._orders_error = false;
    this._orders_loaded = true;
  }

  private handleOrdersLoadError(error: any) {
    this._orders_error = true;
  }

  private resetForm(form: NgForm) {
    this._customer = new Customer(this._clean_customer);

    form.form.markAsPristine();
    form.form.markAsUntouched();
  }
}
