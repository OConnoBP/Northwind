import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CustomerService } from '../../services/customer.service';
import { Customer } from '../../models/customer';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})

export class CustomersComponent implements OnInit {

  private _customers: Customer[] = undefined;
  private _customers_loaded: boolean = false;
  private _error: boolean = false;


  @ViewChild('addCustomerModal') modal: ElementRef;

  constructor(private customer_service: CustomerService) {
    this.loadCustomers();
  }

  ngOnInit() { }

  private loadCustomers(): void {
    this.customer_service.getCustomers().subscribe(data => {
      this.handleLoadCustomerSuccess(data);
    }, error => {
      this.handleLoadCustomerError(error);
    });
  }

  private handleLoadCustomerSuccess(customers: any): void {
    this._customers = customers;

    this._error = false;
    this._customers_loaded = true;
  }

  private handleLoadCustomerError(error: object): void {
    this._error = true;
  }

  openAddCustomerForm() {
    this.modal.nativeElement.openModal();
  }
}
