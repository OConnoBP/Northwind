import { Component, OnInit, Input, Renderer, ElementRef, ViewChild } from '@angular/core';
import { FormsService } from '../forms.service';
import { NgForm } from '@angular/forms';
import { Customer } from '../models/customer';

@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})

export class CustomerFormComponent implements OnInit {

  @Input() customer: any;

  @ViewChild("resetButton") reset_button: ElementRef;
  @ViewChild("form") form: NgForm;

  constructor(private forms_service: FormsService, private renderer: Renderer) {
    //this.customer = new Customer(undefined);
    //console.log(this.customer);
  }

  ngOnInit() {
    /*this.renderer.listen(this.reset_button.nativeElement, 'click', () => {
      this.resetFormService;
    });*/
  }

  resetFormService() {
    //this.forms_service.resetForm(this.form);
  }
}
