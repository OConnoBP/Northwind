import { Injectable } from '@angular/core';
import { NgForm } from '@angular/forms';

@Injectable()

export class FormsService {

  constructor() { }

  public resetForm(form: NgForm) {
    console.log('resetting the form');
    form.form.markAsPristine();
    form.form.markAsUntouched();
  }
}
