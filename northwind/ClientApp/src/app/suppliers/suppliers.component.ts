import { Component, OnInit } from '@angular/core';
import { SupplierService } from '../services/supplier.service';
import { Supplier } from '../models/supplier';

@Component({
  selector: 'app-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.css']
})

export class SuppliersComponent implements OnInit {

  private _suppliers: Supplier[] = undefined;
  private _error: boolean = false;
  private _suppliers_loaded: boolean = false;

  constructor(private supplier_service: SupplierService) {
    this.getSuppliers();
  }

  ngOnInit() { }

  getSuppliers() {
    this.supplier_service.getSuppliers().subscribe(data => {
      this.handleLoadSuppliersSuccess(data);
    }, error => {
      this.handleLoadSuppliersError(error);
    });
  }

  handleLoadSuppliersSuccess(suppliers: any) {
    this._suppliers = suppliers;

    this._error = false;
    this._suppliers_loaded = true;
  }

  handleLoadSuppliersError(error: object) {
    this._error = true;
  }
}
