import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListGridOrderComponent } from './list-grid-order.component';

describe('ListGridOrderComponent', () => {
  let component: ListGridOrderComponent;
  let fixture: ComponentFixture<ListGridOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListGridOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListGridOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
