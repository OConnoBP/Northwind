import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Order } from '../models/order';

@Component({
  selector: 'tr[app-list-grid-order]',
  templateUrl: './list-grid-order.component.html',
  styleUrls: ['./list-grid-order.component.css']
})
export class ListGridOrderComponent implements OnInit {

  @Input() order: Order;

  constructor(private route:ActivatedRoute) { }

  ngOnInit() { }
}
