import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListGridSupplierComponent } from './list-grid-supplier.component';

describe('ListGridSupplierComponent', () => {
  let component: ListGridSupplierComponent;
  let fixture: ComponentFixture<ListGridSupplierComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListGridSupplierComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListGridSupplierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
