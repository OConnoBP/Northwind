import { Component, OnInit, Input } from '@angular/core';
import { Supplier } from '../models/supplier';

@Component({
  selector: 'tr[app-list-grid-supplier]',
  templateUrl: './list-grid-supplier.component.html',
  styleUrls: ['./list-grid-supplier.component.css']
})

export class ListGridSupplierComponent implements OnInit {

  @Input() supplier: Supplier;

  constructor() { }

  ngOnInit() { }
}
