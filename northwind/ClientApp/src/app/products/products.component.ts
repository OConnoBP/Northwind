import { Component, OnInit } from '@angular/core';
import { ProductService } from '../services/product.service';
import { Product } from '../models/Product';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})

export class ProductsComponent implements OnInit {

  private _products: Product[] = undefined;
  private _products_loaded: boolean = false;
  private _error: boolean = false;

  constructor(private product_service: ProductService) {
    this.getProducts();
    product_service.a = true;
  }

  ngOnInit() { }

  getProducts() {
    this.product_service.getProducts().subscribe(data => {
      this.handleProductsLoadSuccess(data);
    }, error => {
      this.handleProductsLoadError(error);
    });
  }

  handleProductsLoadSuccess(products: any) {
    this._products = products;

    this._error = false;
    this._products_loaded = true;
  }

  handleProductsLoadError(error: any) {
    this._error = true;
  }
}
