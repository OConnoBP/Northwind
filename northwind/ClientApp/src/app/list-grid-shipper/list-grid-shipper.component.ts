import { Component, OnInit, Input } from '@angular/core';
import { Shipper } from '../models/shipper';

@Component({
  selector: 'tr[app-list-grid-shipper]',
  templateUrl: './list-grid-shipper.component.html',
  styleUrls: ['./list-grid-shipper.component.css']
})

export class ListGridShipperComponent implements OnInit {

  @Input() shipper: Shipper;

  constructor() { }

  ngOnInit() {
  }

}
