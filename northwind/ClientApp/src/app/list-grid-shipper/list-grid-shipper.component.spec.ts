import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListGridShipperComponent } from './list-grid-shipper.component';

describe('ListGridShipperComponent', () => {
  let component: ListGridShipperComponent;
  let fixture: ComponentFixture<ListGridShipperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListGridShipperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListGridShipperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
