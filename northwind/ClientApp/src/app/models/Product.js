"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Category_1 = require("./Category");
var supplier_1 = require("./supplier");
var Product = /** @class */ (function () {
    function Product(product) {
        this.productId = product.productId || undefined;
        this.productName = product.productName || undefined;
        this.supplier = new supplier_1.Supplier(product.supplier);
        this.category = new Category_1.Category(product.category);
        this.quantityPerUnit = product.quantityPerUnit || undefined;
        this.unitPrice = product.unitPrice || undefined;
        this.unitsInStock = product.unitsInStock || undefined;
        this.unitsOnOrder = product.unitsOnOrder || undefined;
        this.reorderLevel = product.reorderLevel || undefined;
        this.discontinued = product.discontinued || undefined;
    }
    Object.defineProperty(Product.prototype, "productId", {
        get: function () {
            return this._product_id;
        },
        set: function (product_id) {
            this._product_id = product_id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Product.prototype, "productName", {
        get: function () {
            return this._product_name;
        },
        set: function (product_name) {
            this._product_name = product_name;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Product.prototype, "supplier", {
        get: function () {
            return this._supplier;
        },
        set: function (supplier) {
            this._supplier = supplier;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Product.prototype, "category", {
        get: function () {
            return this._category;
        },
        set: function (category) {
            this._category = category;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Product.prototype, "quantityPerUnit", {
        get: function () {
            return this._quantity_per_unit;
        },
        set: function (quantity_per_unit) {
            this._quantity_per_unit = quantity_per_unit;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Product.prototype, "unitPrice", {
        get: function () {
            return this._unit_price;
        },
        set: function (unit_price) {
            this._unit_price = unit_price;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Product.prototype, "unitsInStock", {
        get: function () {
            return this._units_in_stock;
        },
        set: function (units_in_stock) {
            this._units_in_stock = units_in_stock;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Product.prototype, "unitsOnOrder", {
        get: function () {
            return this._units_on_order;
        },
        set: function (units_on_order) {
            this._units_on_order = units_on_order;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Product.prototype, "reorderLevel", {
        get: function () {
            return this._reorder_level;
        },
        set: function (reorder_level) {
            this._reorder_level = reorder_level;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Product.prototype, "discontinued", {
        get: function () {
            return this._discontinued;
        },
        set: function (discontinued) {
            this._discontinued = discontinued;
        },
        enumerable: true,
        configurable: true
    });
    return Product;
}());
exports.Product = Product;
//# sourceMappingURL=Product.js.map