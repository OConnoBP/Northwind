export class Customer {
  private _customer_id: number;
  private _company_name: string;
  private _contact_name: string;
  private _contact_title: string;
  private _address: string;
  private _city: string;
  private _region: string;
  private _postal_code: string;
  private _country: string;
  private _phone: string;
  private _fax: string;
  private _bool: boolean;

  constructor(customer: any) {
    this.customerId = customer.customerId;
    this.companyName = customer.companyName;
    this.contactName = customer.contactName;
    this.contactTitle = customer.contactTitle;
    this.address = customer.address;
    this.city = customer.city;
    this.region = customer.region;
    this.postalCode = customer.postalCode;
    this.country = customer.country;
    this.phone = customer.phone;
    this.fax = customer.fax;
    this.bool = customer.bool;
  }

  get customerId(): number {
    return this._customer_id;
  }

  set customerId(customer_id: number) {
    this._customer_id = customer_id;
  }

  get companyName(): string {
    return this._company_name;
  }

  set companyName(company_name: string) {
    this._company_name = company_name;
  }

  get contactName(): string {
    return this._contact_name;
  }

  set contactName(contact_name: string) {
    this._contact_name = contact_name
  }

  get contactTitle(): string {
    return this._contact_title;
  }

  set contactTitle(contact_title: string) {
    this._contact_title = contact_title;
  }

  get address(): string {
    return this._address;
  }

  set address(address: string) {
    this._address = address;
  }

  get city(): string {
    return this._city;
  }

  set city(city: string) {
    this._city = city;
  }

  get region(): string {
    return this._region;
  }

  set region(region: string) {
    this._region = region;
  }

  get postalCode(): string {
    return this._postal_code;
  }

  set postalCode(postal_code: string) {
    this._postal_code = postal_code;
  }

  get country(): string {
    return this._country;
  }

  set country(country: string) {
    this._country = country;
  }

  get phone(): string {
    return this._phone;
  }

  set phone(phone: string) {
    this._phone = phone;
  }

  get fax(): string {
    return this._fax;
  }

  set fax(fax: string) {
    this._fax = fax;
  }

  get bool(): boolean {
    return this._bool;
  }

  set bool(bool: boolean) {
    this._bool = bool;
  }
}
