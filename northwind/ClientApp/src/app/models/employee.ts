export class Employee {
  private _employee_id: number;
  private _last_name: string;
  private _first_name: string;
  private _full_name: string;
  private _title: string;
  private _title_of_courtesy: string;
  private _birth_date: string;
  private _hire_date: string;
  private _address: string;
  private _city: string;
  private _region: string;
  private _postal_code: string;
  private _country: string;
  private _home_phone: string;
  private _extension: string;
  private _photo: string;
  private _notes: string;
  private _supervisor: Employee;
  private _photo_path: string;
  private _orders: any;

  constructor(employee: any) {
    this.employeeId = employee.employeeId;
    this.lastName = employee.lastName;
    this.firstName = employee.firstName;
    this.title = employee.title;
    this.titleOfCourtesy = employee.titleOfCourtesy;
    this.birthDate = employee.birthDate;
    this.hireDate = employee.hireDate;
    this.address = employee.address;
    this.city = employee.city;
    this.region = employee.region;
    this.postalCode = employee.postalCode;
    this.country = employee.country;
    this.homePhone = employee.homePhone;
    this.extension = employee.extension;
    this.photo = employee.photo;
    this.notes = employee.notes;
    this.supervisor = employee.supervisor !== null ? new Employee(employee.supervisor) : null;
    this.photoPath = employee.photoPath;
    this.orders = employee.orders;
  }

  get employeeId(): number {
    return this._employee_id;
  }

  set employeeId(employee_id: number) {
    this._employee_id = employee_id;
  }

  get lastName(): string {
    return this._last_name;
  }

  set lastName(last_name: string) {
    this._last_name = last_name;
  }

  get firstName(): string {
    return this._first_name;
  }

  set firstName(first_name: string) {
    this._first_name = first_name;
  }

  get fullName(): string {
    return this.titleOfCourtesy + " " + this.firstName + " " + this.lastName;
  }

  get title(): string {
    return this._title;
  }

  set title(title: string) {
    this._title = title;
  }

  get titleOfCourtesy(): string {
    return this._title_of_courtesy;
  }

  set titleOfCourtesy(title_of_courtesy: string) {
    this._title_of_courtesy = title_of_courtesy;
  }

  get birthDate(): string {
    return this._birth_date;
  }

  set birthDate(birth_date: string) {
    this._birth_date = birth_date;
  }

  get hireDate(): string {
    return this._hire_date;
  }

  set hireDate(hire_date: string) {
    this._hire_date = hire_date;
  }

  get address(): string {
    return this._address;
  }

  set address(address: string) {
    this._address = address;
  }

  get city(): string {
    return this._city;
  }

  set city(city: string) {
    this._city = city;
  }

  get region(): string {
    return this._region;
  }

  set region(region: string) {
    this._region = region;
  }

  get postalCode(): string {
    return this._postal_code;
  }

  set postalCode(postal_code: string) {
    this._postal_code = postal_code;
  }

  get country(): string {
    return this._country;
  }

  set country(country: string) {
    this._country = country;
  }

  get homePhone(): string {
    return this._home_phone;
  }

  set homePhone(home_phone: string) {
    this._home_phone = home_phone;
  }

  get extension(): string {
    return this._extension;
  }

  set extension(extension: string) {
    this._extension = extension;
  }

  get photo(): string {
    return this._photo;
  }

  set photo(photo: string) {
    this._photo = photo;
  }

  get notes(): string {
    return this._notes;
  }

  set notes(notes: string) {
    this._notes = notes;
  }

  get supervisor(): any {
    return this._supervisor;
  }

  set supervisor(supervisor: any) {
    this._supervisor = supervisor;
  }

  get photoPath(): string {
    return this._photo_path;
  }

  set photoPath(photo_path: string) {
    this._photo_path = photo_path;
  }

  get orders(): any {
    return this._orders;
  }

  set orders(orders: any) {
    this._orders = orders;
  }
}
