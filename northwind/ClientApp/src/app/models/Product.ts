import { Category } from "./Category";
import { Supplier } from './supplier';

export class Product {
  private _product_id: number;
  private _product_name: string;
  private _supplier: object;
  private _category: object;
  private _quantity_per_unit: string;
  private _unit_price: number;
  private _units_in_stock: number;
  private _units_on_order: number;
  private _reorder_level: number;
  private _discontinued: boolean;

  constructor(product: any) {
    this.productId = product.productId || undefined;
    this.productName = product.productName || undefined;
    this.supplier = new Supplier(product.supplier);
    this.category = new Category(product.category);
    this.quantityPerUnit = product.quantityPerUnit || undefined;
    this.unitPrice = product.unitPrice || undefined;
    this.unitsInStock = product.unitsInStock || undefined;
    this.unitsOnOrder = product.unitsOnOrder || undefined;
    this.reorderLevel = product.reorderLevel || undefined;
    this.discontinued = product.discontinued || undefined;
  }

  get productId(): number {
    return this._product_id;
  }

  set productId(product_id: number) {
    this._product_id = product_id;
  }

  get productName(): string {
    return this._product_name;
  }

  set productName(product_name: string) {
    this._product_name = product_name;
  }

  get supplier(): object {
    return this._supplier;
  }

  set supplier(supplier: object) {
    this._supplier = supplier;
  }

  get category(): object {
    return this._category;
  }

  set category(category: object) {
    this._category = category;
  }

  get quantityPerUnit(): string {
    return this._quantity_per_unit;
  }

  set quantityPerUnit(quantity_per_unit: string) {
    this._quantity_per_unit = quantity_per_unit;
  }

  get unitPrice(): number {
    return this._unit_price;
  }

  set unitPrice(unit_price: number) {
    this._unit_price = unit_price;
  }

  get unitsInStock(): number {
    return this._units_in_stock;
  }

  set unitsInStock(units_in_stock: number) {
    this._units_in_stock = units_in_stock;
  }

  get unitsOnOrder(): number {
    return this._units_on_order;
  }

  set unitsOnOrder(units_on_order: number) {
    this._units_on_order = units_on_order;
  }

  get reorderLevel(): number {
    return this._reorder_level;
  }

  set reorderLevel(reorder_level: number) {
    this._reorder_level = reorder_level;
  }

  get discontinued(): boolean {
    return this._discontinued;
  }

  set discontinued(discontinued: boolean) {
    this._discontinued = discontinued;
  }
}
