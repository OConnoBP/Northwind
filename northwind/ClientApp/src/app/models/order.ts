import { Customer } from './customer';
import { Employee } from './employee';
import { Shipper } from './shipper';

export class Order {
  private _order_id: number;
  private _customer: any;
  private _employee: any;
  private _order_date: string;
  private _required_date: string;
  private _shipped_date: string;
  private _shipper: any;
  private _freight: number;
  private _ship_name: string;
  private _ship_address: string;
  private _ship_city: string;
  private _ship_region: string;
  private _ship_postal_code: string;
  private _ship_country: string;
  private _order_details: any;

  constructor(order: any) {
    this.orderId = order.orderId;
    this.customer = new Customer(order.customer);
    this.employee = new Employee(order.employee);
    this.orderDate = order.orderDate;
    this.requiredDate = order.requiredDate;
    this.shippedDate = order.shippedDate;
    this.shipper = new Shipper(order.shipper);
    this.freight = order.freight;
    this.shipName = order.shipName;
    this.shipAddress = order.shipAddress;
    this.shipCity = order.shipCity;
    this.shipRegion = order.shipRegion;
    this.shipPostalCode = order.shipPostalCode;
    this.shipCountry = order.shipCountry;
    this.orderDetails = order.orderDetails;
  }

  get orderId(): number {
    return this._order_id;
  }

  set orderId(order_id: number) {
    this._order_id = order_id;
  }

  get customer(): any {
    return this._customer;
  }

  set customer(customer: any) {
    this._customer = customer;
  }

  get employee(): any {
    return this._employee;
  }

  set employee(employee: any) {
    this._employee = employee;
  }

  get orderDate(): string {
    return this._order_date;
  }

  set orderDate(order_date: string) {
    this._order_date = order_date;
  }

  get requiredDate(): string {
    return this._required_date;
  }

  set requiredDate(required_date: string) {
    this._required_date = required_date;
  }

  get shippedDate(): string {
    return this._shipped_date;
  }

  set shippedDate(shipped_date: string) {
    this._shipped_date = shipped_date;
  }

  get shipper(): any {
    return this._shipper;
  }

  set shipper(shipper: any) {
    this._shipper = shipper;
  }

  get freight(): number {
    return this._freight;
  }

  set freight(freight: number) {
    this._freight = freight;
  }

  get shipName(): string {
    return this._ship_name;
  }

  set shipName(ship_name: string) {
    this._ship_name = ship_name;
  }

  get shipAddress(): string {
    return this._ship_address;
  }

  set shipAddress(ship_address: string) {
    this._ship_address = ship_address;
  }

  get shipCity(): string {
    return this._ship_city;
  }

  set shipCity(ship_city: string) {
    this._ship_city = ship_city;
  }

  get shipRegion(): string {
    return this._ship_region;
  }

  set shipRegion(ship_region: string) {
    this._ship_region = ship_region;
  }

  get shipPostalCode(): string {
    return this._ship_postal_code;
  }

  set shipPostalCode(ship_postal_code: string) {
    this._ship_postal_code = ship_postal_code;
  }

  get shipCountry(): string {
    return this._ship_country;
  }

  set shipCountry(ship_country: string) {
    this._ship_country = ship_country;
  }

  get orderDetails(): any {
    return this._order_details;
  }

  set orderDetails(order_details: any) {
    this._order_details = order_details;
  }
}
