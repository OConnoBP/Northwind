"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Category = /** @class */ (function () {
    function Category(category) {
        console.log(category);
        this.categoryId = category.categoryId;
        this.categoryName = category.categoryName;
        this.description = category.description;
        this.picture = category.picture;
    }
    Object.defineProperty(Category.prototype, "categoryId", {
        get: function () {
            return this._category_id;
        },
        set: function (category_id) {
            this._category_id = category_id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Category.prototype, "categoryName", {
        get: function () {
            return this._category_name;
        },
        set: function (category_name) {
            this._category_name = category_name;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Category.prototype, "description", {
        get: function () {
            return this._description;
        },
        set: function (description) {
            this._description = description;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Category.prototype, "picture", {
        get: function () {
            return this._picture;
        },
        set: function (picture) {
            this._picture = picture;
        },
        enumerable: true,
        configurable: true
    });
    Category.prototype.toJSON = function () {
        return {
            CategoryId: this.categoryId,
            CategoryName: this.categoryName,
            Description: this.description,
            Picture: this.picture
        };
    };
    return Category;
}());
exports.Category = Category;
//# sourceMappingURL=Category.js.map