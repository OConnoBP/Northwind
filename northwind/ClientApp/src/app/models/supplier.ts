export class Supplier {
  private _supplier_id: number;
  private _company_name: string;
  private _contact_name: string;
  private _contact_title: string;
  private _address: string;
  private _city: string;
  private _region: string;
  private _postal_code: string;
  private _country: string;
  private _phone: string;
  private _fax: string;
  private _home_page: string;
  private _products: any;

  constructor(supplier: any) {
    this.supplierId = supplier.supplierId;
    this.companyName = supplier.companyName;
    this.contactName = supplier.contactName;
    this.contactTitle = supplier.contactTitle;
    this.address = supplier.address;
    this.city = supplier.city;
    this.region = supplier.region;
    this.postalCode = supplier.postalCode;
    this.country = supplier.country;
    this.phone = supplier.phone;
    this.fax = supplier.fax;
    this.homePage = supplier.homePage;
    this.products = supplier.products;
  }

  get supplierId(): number {
    return this._supplier_id;
  }

  set supplierId(supplier_id) {
    this._supplier_id = supplier_id;
  }

  get companyName(): string {
    return this._company_name;
  }

  set companyName(company_name: string) {
    this._company_name = company_name;
  }

  get contactName(): string {
    return this._contact_name;
  }

  set contactName(contact_name: string) {
    this._contact_name = contact_name;
  }

  get contactTitle(): string {
    return this._contact_title;
  }

  set contactTitle(contact_title: string) {
    this._contact_title = contact_title;
  }

  get address(): string {
    return this._address;
  }

  set address(address: string) {
    this._address = address;
  }

  get city(): string {
    return this._city;
  }

  set city(city: string) {
    this._city = city;
  }

  get region(): string {
    return this._region;
  }

  set region(region: string) {
    this._region = region;
  }

  get postalCode(): string {
    return this._postal_code;
  }

  set postalCode(postal_code: string) {
    this._postal_code = postal_code;
  }

  get country(): string {
    return this._country;
  }

  set country(country: string) {
    this._country = country;
  }

  get phone(): string {
    return this._phone;
  }

  set phone(phone: string) {
    this._phone = phone;
  }

  get fax(): string {
    return this._fax;
  }

  set fax(fax: string) {
    this._fax = fax;
  }

  get homePage(): string {
    return this._home_page;
  }

  set homePage(home_page: string) {
    this._home_page = home_page;
  }

  get products(): any {
    return this._products;
  }

  set products(products: any) {
    this._products = products;
  }
}
