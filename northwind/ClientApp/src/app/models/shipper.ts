export class Shipper {
  private _shipper_id: number;
  private _company_name: string;
  private _phone: string;

  constructor(shipper: any) {
    console.log(shipper);
    this.shipperId = shipper.shipperId;
    this.companyName = shipper.companyName;
    this.phone = shipper.phone;
  }

  get shipperId(): number {
    return this._shipper_id;
  }

  set shipperId(shipper_id: number) {
    this._shipper_id = shipper_id;
  }

  get companyName(): string {
    return this._company_name;
  }

  set companyName(company_name: string) {
    this._company_name = company_name;
  }

  get phone(): string {
    return this._phone;
  }

  set phone(phone: string) {
    this._phone = phone;
  }
}
