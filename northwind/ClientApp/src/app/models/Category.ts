export class Category {
  private _category_id;
  private _category_name;
  private _description;
  private _picture;

  constructor(category: any) {
    this.categoryId = category.categoryId;
    this.categoryName = category.categoryName;
    this.description = category.description;
    this.picture = category.picture;
  }

  get categoryId() {
    return this._category_id;
  }

  set categoryId(category_id: number) {
    this._category_id = category_id;
  }

  get categoryName() {
    return this._category_name;
  }

  set categoryName(category_name: string) {
    this._category_name = category_name;
  }

  get description() {
    return this._description;
  }

  set description(description: string) {
    this._description = description;
  }

  get picture() {
    return this._picture;
  }

  set picture(picture: string) {
    this._picture = picture;
  }

  toJSON(): object {
    return {
      CategoryId: this.categoryId,
      CategoryName: this.categoryName,
      Description: this.description,
      Picture: this.picture
    }
  }
}
