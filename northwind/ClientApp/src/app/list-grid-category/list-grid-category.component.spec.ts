import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListGridCategoryComponent } from './list-grid-category.component';

describe('ListGridCategoryComponent', () => {
  let component: ListGridCategoryComponent;
  let fixture: ComponentFixture<ListGridCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListGridCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListGridCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
