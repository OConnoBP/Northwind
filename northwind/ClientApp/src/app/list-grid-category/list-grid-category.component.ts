import { Component, OnInit, Input } from '@angular/core';
import { Category } from '../models/Category';

@Component({
  selector: 'tr[app-list-grid-category]',
  templateUrl: './list-grid-category.component.html',
  styleUrls: ['./list-grid-category.component.css']
})

export class ListGridCategoryComponent implements OnInit {

  @Input() category: Category;

  constructor() { }

  ngOnInit() { }
}
