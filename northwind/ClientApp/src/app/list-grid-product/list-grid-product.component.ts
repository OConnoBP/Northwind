import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from '../models/Product';

@Component({
  selector: 'tr[app-list-grid-product]',
  templateUrl: './list-grid-product.component.html',
  styleUrls: ['./list-grid-product.component.css']
})
export class ListGridProductComponent implements OnInit {

  @Input() product: Product;

  constructor(private route:ActivatedRoute) { }

  ngOnInit() { }

  showWarning(): boolean {
    if (!this.product.discontinued && (this.product.unitsInStock + this.product.unitsOnOrder) < this.product.reorderLevel) {
      //  If the product is not discontinued and the number of units in stock + units on order is less than the reorder level
      return true;
    } else {
      return false;
    }
  }
}
