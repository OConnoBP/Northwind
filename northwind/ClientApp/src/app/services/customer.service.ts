import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Customer } from '../models/customer';

@Injectable()

export class CustomerService {

  constructor(private http: HttpClient) { }

  getCustomers(): Observable<object> {
    return this.http.get('/api/Customers').map((arr: Array<object>) =>
      arr.map((customer: any) =>
        new Customer(customer)
      )
    );
  }

  getCustomer(id: string): Observable<object> {
    return this.http.get(`/api/Customers/GetCustomerByCustomerId?customerId=${id}`).map((customer: any) =>
      new Customer(customer)
    );
  }
}
