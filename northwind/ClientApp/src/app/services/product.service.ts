import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Product } from '../models/Product';
import 'rxjs/add/operator/map';

@Injectable()

export class ProductService {

  a: boolean = false;

  constructor(private http: HttpClient) { }

  getProducts(): Observable<object> {
    return this.http.get('/api/Products').map((arr: Array<object>) =>
      arr.map((product: any) =>
        new Product(product)
      )
    );
  }

  getProductByProductId(productId: string): Observable<object> {
    return this.http.get(`/api/Products/GetProductByProductId?productId=${productId}`).map((product: any) =>
      new Product(product)
    );
  }

  getProductsByCategoryId(categoryId: string): Observable<object> {
    return this.http.get(`/api/Products/GetProductsByCategoryId?categoryId=${categoryId}`).map((arr: Array<object>) =>
      arr.map((product: any) =>
        new Product(product)
      )
    );
  }

  getProductsBySupplierId(supplierId: string): Observable<object> {
    return this.http.get(`/api/Products/GetProductsBySupplierId?supplierId=${supplierId}`).map((arr: Array<object>) =>
      arr.map((product: any) =>
        new Product(product)
      )
    );
  }

  getProductsByOrderId(orderId: number): Observable<object> {
    return this.http.get(`/api/Products/GetProductsByOrderId?orderId=${orderId}`).map((arr: Array<object>) =>
      arr.map((product: any) =>
        new Product(product)
      )
    );
  }
}
