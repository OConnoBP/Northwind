import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Category } from '../models/Category';
import 'rxjs/add/operator/map';

@Injectable()

export class CategoryService {

  constructor(private http: HttpClient) { }

  getCategories(): Observable<object> {
    return this.http.get('/api/Categories').map((arr: Array<object>) =>
      arr.map((category: any) =>
        new Category(category)
      )
    );
  }

  getCategory(id: string): Observable<object> {
    return this.http.get(`/api/Categories/GetCategoryByCategoryId?categoryId=${id}`).map((category: any) =>
      new Category(category)
    );
  }

  updateCategory(category: object): Observable<object> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf8' });
    return this.http.post('/api/Categories/UpdateCategory', category, { headers: headers });
  }
}
