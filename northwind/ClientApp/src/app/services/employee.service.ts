import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { HttpClient } from '@angular/common/http';
import { Employee } from '../models/employee';

@Injectable()

export class EmployeeService {

  constructor(private http:HttpClient) { }

  getEmployeeByEmployeeId(id: number): Observable<object> {
    return this.http.get(`/api/Employees/GetEmployeeByEmployeeId?employeeId=${id}`).map((employee: any) =>
      new Employee(employee)
    );
  }

  getEmployees(): Observable<object> {
    return this.http.get('/api/Employees/GetEmployees').map((arr: Array<object>) =>
      arr.map((employee: Employee) =>
        new Employee(employee)
      )
    );
  }
}
