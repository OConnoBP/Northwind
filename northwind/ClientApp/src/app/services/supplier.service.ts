import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Supplier } from '../models/supplier';

@Injectable()

export class SupplierService {

  constructor(private http:HttpClient) { }

  getSuppliers() {
    return this.http.get('/api/Suppliers/GetSuppliers').map((arr: Array<object>) =>
      arr.map((supplier: any) =>
        new Supplier(supplier)
      )
    );
  }

  getSupplierBySupplierId(id: string) {
    return this.http.get(`/api/Suppliers/GetSupplierBySupplierId?supplierId=${id}`).map((supplier: any) =>
      new Supplier(supplier)
    );
  }
}
