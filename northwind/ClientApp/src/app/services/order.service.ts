import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Order } from '../models/order';

@Injectable()

export class OrderService {

  constructor(private http: HttpClient) { }

  getOrders(): Observable<object> {
    return this.http.get('api/Orders/GetOrders').map((arr: Array<object>) =>
      arr.map((order: any) =>
        new Order(order)
      )
    );
  }

  getOrderByOrderId(id: number): Observable<object> {
    return this.http.get(`api/Orders/GetOrderByOrderId?orderId=${id}`).map((order: any) =>
      new Order(order)
    );
  }

  getOrdersByCustomerId(id: string): Observable<object> {
    return this.http.get(`api/Orders/GetOrdersByCustomerId?customerId=${id}`).map((arr: Array<object>) =>
      arr.map((order: any) =>
        new Order(order)
      )
    );
  }

  getOrdersByEmployeeId(id: number): Observable<object> {
    return this.http.get(`api/Orders/GetOrdersByEmployeeId?employeeId=${id}`).map((arr: Array<object>) =>
      arr.map((order: any) =>
        new Order(order)
      )
    );
  }
}
