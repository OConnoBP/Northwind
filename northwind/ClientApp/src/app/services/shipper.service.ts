import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Shipper } from '../models/shipper';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()

export class ShipperService {

  constructor(private http: HttpClient) { }

  getShippers(): Observable<object> {
    console.log('a');
    return this.http.get('api/Shippers/GetShippers').map((arr: Array<object>) =>
      arr.map((shipper: any) =>
        new Shipper(shipper)
      )
    );
  }
}
