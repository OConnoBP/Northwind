import { Component, OnInit, Input } from '@angular/core';
import { Customer } from '../models/customer';

@Component({
  selector: 'tr[app-list-grid-customer]',
  templateUrl: './list-grid-customer.component.html',
  styleUrls: ['./list-grid-customer.component.css']
})
export class ListGridCustomerComponent implements OnInit {
  @Input() customer: Customer;

  constructor() { }

  ngOnInit() { }
}
