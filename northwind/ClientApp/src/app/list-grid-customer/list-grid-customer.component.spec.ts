import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListGridCustomerComponent } from './list-grid-customer.component';

describe('ListGridCustomerComponent', () => {
  let component: ListGridCustomerComponent;
  let fixture: ComponentFixture<ListGridCustomerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListGridCustomerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListGridCustomerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
