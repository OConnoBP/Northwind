import { Directive, OnInit, ElementRef } from '@angular/core';

@Directive({
  selector: '[appModal]'
})
export class ModalDirective implements OnInit {

  constructor(private el: ElementRef) { }

  ngOnInit() {
    this.el.nativeElement.addEventListener
  }


  hideModal(scope, element, attrs): void {
    scope.showModal = visible => {
      if (visible) {
        element.modal("show");
      } else {
        element.modal("hide");
      }
    }
  }
}
