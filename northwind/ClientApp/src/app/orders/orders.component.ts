import { Component, OnInit } from '@angular/core';
import { OrderService } from '../services/order.service';
import { Order } from '../models/order';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})

export class OrdersComponent implements OnInit {

  private _orders: Order[];
  private _orders_loaded: boolean = false;
  private _error: boolean = false;

  constructor(private order_service: OrderService) {
    this.getOrders();
  }

  ngOnInit() { }

  getOrders() {
    this.order_service.getOrders().subscribe(data => {
      this.handleOrdersLoadSuccess(data);
    }, error => {
      this.handleOrdersLoadError(error);
    });
  }

  handleOrdersLoadSuccess(orders: any) {
    this._orders = orders;

    this._orders_loaded = true;
    this._error = false;
  }

  handleOrdersLoadError(error: object) {
    this._error = true;
  }
}
