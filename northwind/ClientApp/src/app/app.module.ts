import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MzButtonModule, MzModalModule, MzTextareaModule, MzNavbarModule, MzSidenavModule, MzToastModule, MzSpinnerModule, MzTooltipModule, MzPaginationModule, MzSelectModule, MzInputModule, MzCheckboxModule, MzCollapsibleModule, MzCollectionModule } from 'ngx-materialize';

import { DataTablesModule } from 'angular-datatables';

//  PLACE ALL PAGE COMPONENTS UNDERNEATH HERE  //
import { HomeComponent } from './home/home.component';
import { CategoriesComponent } from './pages/categories/categories.component';
import { CustomersComponent } from './pages/customers/customers.component';
import { ProductsComponent } from './products/products.component';
import { CategoryDetailsComponent } from './pages/category-details/category-details.component';
import { CustomerDetailsComponent } from './pages/customer-details/customer-details.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { SuppliersComponent } from './suppliers/suppliers.component';
import { SupplierDetailsComponent } from './supplier-details/supplier-details.component';
import { EmployeesComponent } from './employees/employees.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { OrdersComponent } from './orders/orders.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { ShippersComponent } from './shippers/shippers.component';
import { ShipperDetailsComponent } from './shipper-details/shipper-details.component';

//  PLACE ALL SERVICES UNDERNEATH HERE  //
import { CategoryService } from './services/category.service';
import { CustomerService } from './services/customer.service';
import { ProductService } from './services/product.service';
import { OrderService } from './services/order.service';
import { SupplierService } from './services/supplier.service';
import { EmployeeService } from './services/employee.service';


//  PLACE ALL COMPONENTS UNDERNEATH HERE  //
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';

import { ListGridComponent } from './list-grid/list-grid.component';

import { ErrorComponent } from './error/error.component';
import { ModalDirective } from './modal.directive';
import { ListGridProductComponent } from './list-grid-product/list-grid-product.component';
import { ListGridCategoryComponent } from './list-grid-category/list-grid-category.component';
import { ListGridCustomerComponent } from './list-grid-customer/list-grid-customer.component';
import { ListGridSupplierComponent } from './list-grid-supplier/list-grid-supplier.component';
import { ListGridEmployeeComponent } from './list-grid-employee/list-grid-employee.component';
import { ListGridOrderComponent } from './list-grid-order/list-grid-order.component';
import { ListGridShipperComponent } from './list-grid-shipper/list-grid-shipper.component';

import { LoadingComponent } from './loading/loading.component';
import { ShipperService } from './services/shipper.service';
import { CustomerFormComponent } from './customer-form/customer-form.component';
import { FormsService } from './forms.service';



@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    ListGridComponent,
    CategoriesComponent,
    CustomersComponent,
    ProductsComponent,
    CategoryDetailsComponent,
    CustomerDetailsComponent,
    ProductDetailsComponent,
    SuppliersComponent,
    SupplierDetailsComponent,
    EmployeesComponent,
    EmployeeDetailsComponent,
    OrdersComponent,
    OrderDetailsComponent,
    ShippersComponent,
    ErrorComponent,
    ModalDirective,
    ListGridProductComponent,
    ListGridCategoryComponent,
    ListGridCustomerComponent,
    ListGridSupplierComponent,
    ListGridEmployeeComponent,
    ListGridOrderComponent,
    ListGridShipperComponent,
    LoadingComponent,
    ShipperDetailsComponent,
    CustomerFormComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    BrowserAnimationsModule,
    HttpClientModule,
    DataTablesModule,
    MzButtonModule,
    MzModalModule,
    MzTextareaModule,
    MzNavbarModule,
    MzSidenavModule,
    MzToastModule,
    MzSpinnerModule,
    MzTooltipModule,
    MzPaginationModule,
    MzSelectModule,
    MzInputModule,
    MzCheckboxModule,
    MzCollapsibleModule,
    MzCollectionModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'categories', component: CategoriesComponent },
      { path: 'categories/:id', component: CategoryDetailsComponent },
      { path: 'customers', component: CustomersComponent },
      { path: 'customers/:id', component: CustomerDetailsComponent },
      { path: 'products', component: ProductsComponent },
      { path: 'products/:id', component: ProductDetailsComponent },
      { path: 'suppliers', component: SuppliersComponent },
      { path: 'suppliers/:id', component: SupplierDetailsComponent },
      { path: 'employees', component: EmployeesComponent },
      { path: 'employees/:id', component: EmployeeDetailsComponent },
      { path: 'orders', component: OrdersComponent },
      { path: 'orders/:id', component: OrderDetailsComponent },
      { path: 'shippers', component: ShippersComponent },
      { path: 'shippers/:id', component: ShipperDetailsComponent }
    ])
  ],
  providers: [
    CategoryService,
    CustomerService,
    ProductService,
    OrderService,
    SupplierService,
    EmployeeService,
    ShipperService,
    FormsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
