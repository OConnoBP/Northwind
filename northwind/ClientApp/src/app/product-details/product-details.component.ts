import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../services/product.service';
import { Product } from '../models/Product';
import { SupplierService } from '../services/supplier.service';
import { Supplier } from '../models/supplier';
import { Category } from '../models/Category';
import { CategoryService } from '../services/category.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})

export class ProductDetailsComponent implements OnInit {

  private _product: Product = undefined;
  private _error: boolean = false;
  private _product_loaded: boolean = false;

  private _suppliers: Supplier[] = undefined;

  private _categories: any = undefined;

  constructor(private route: ActivatedRoute, private product_service: ProductService, private supplier_service: SupplierService, private category_service: CategoryService) {
    this.loadProducts();
    this.loadSuppliers();
    this.loadCategories();
  }

  ngOnInit() { }

  private loadProducts(): void {
    this.product_service.getProductByProductId(this.route.snapshot.params['id']).subscribe(data => {
      this.handleProductsLoadSuccess(data);
    }, error => {
      this.handleProductsLoadError(error);
    });
  }

  private handleProductsLoadSuccess(product: any): void {
    this._product = product;

    this._error = false;
    this._product_loaded = true;
  }

  private handleProductsLoadError(error: object): void {
    this._error = true;
  }

  private loadSuppliers(): void {
    this.supplier_service.getSuppliers().subscribe(data => {
      this._suppliers = data;
    });
  }

  private loadCategories(): void {
    this.category_service.getCategories().subscribe(data => {
      this._categories = data;
    });
  }

  compareCategories(c1: Category, c2: Category) {
    return c1 && c2 ? c1.categoryName === c2.categoryName : c1 === c2;
  }

  compareSuppliers(s1: Supplier, s2: Supplier) {
    return s1 && s2 ? s1.companyName === s2.companyName : s1 === s2;
  }
}
