﻿using System.Collections.Generic;

namespace northwind.Models
{
    public class Product
    {
        private int _units_in_stock;
        private int _units_on_order;
        private int _reorder_level;

        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public Supplier Supplier { get; set; }
        public Category Category { get; set; }
        public string QuantityPerUnit { get; set; }
        public decimal UnitPrice { get; set; }

        public int UnitsInStock
        { 
            get
            {
                return _units_in_stock;
            }
            set
            {
                _units_in_stock = value;
                warning();
            }
        }

        public int UnitsOnOrder
        {
            get
            {
                return _units_on_order;
            }
            set
            {
                _units_on_order = value;
                warning();
            }
        }

        public int ReorderLevel
        {
            get
            {
                return _reorder_level;
            }
            set
            {
                _reorder_level = value;
                warning();
            }
        }

        public bool ShowReorderWarning { get; set; }

        private void warning()
        {
            //  Only change these booleans if the item is not discontinued
            if(Discontinued == false)
            {
                if (UnitsInStock < ReorderLevel && UnitsInStock != 0)
                {
                    IsLowStock = true;
                }
                else if (UnitsInStock == 0)
                {
                    IsOutOfStock = true;
                }
            }
            
        }

        public bool Discontinued { get; set; }
        public bool IsLowStock { get; set; }
        public bool IsOutOfStock { get; set; }
    }
}
