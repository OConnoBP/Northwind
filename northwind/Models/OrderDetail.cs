﻿namespace northwind.Models
{
    public class OrderDetail
    {
        public int OrderId { get; set; }
        public Product Product { get; set; }
        public decimal OrderDetailsUnitPrice { get; set; }
        public int Quantity { get; set; }
        public double Discount { get; set; }
    }
}
