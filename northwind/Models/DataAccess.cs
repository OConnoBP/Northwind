﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace northwind.Models
{
    public class DataAccess
    {
        public static T get_value<T>(string sql, params object[] parameters)
        {
            var value = get_value(sql, parameters);
            if (value is DBNull) return default(T);
            return (T)value;
        }

        public static object get_value(string sql, params object[] parameters)
        {
            object return_value;
            using (var conn = get_open_connection())
            {
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = sql;

                    for (int i = 0; i < parameters.Length; i++)
                    {
                        cmd.Parameters.Add(new SqlParameter(i.ToString(), parameters[i]));
                    }

                    try
                    {
                        return_value = cmd.ExecuteScalar();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
            }
            return return_value;
        }

        public static void execute(string sql, params object[] parameters)
        {
            using (var conn = get_open_connection())
            {
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = sql;

                    for (int i = 0; i < parameters.Length; i++)
                    {
                        cmd.Parameters.Add(new SqlParameter(i.ToString(), parameters[i]));
                    }

                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
            }
        }

        public static int count(string table, Dictionary<string, object> conditions)
        {
            object return_value;
            using (var conn = get_open_connection())
            {
                using (var cmd = new SqlCommand())
                {
                    StringBuilder conds = new StringBuilder();

                    foreach (string key in conditions.Keys)
                    {
                        if (conds.Length > 0) conds.Append(" AND ");
                        conds.AppendFormat("{0} = @c{0}", key);
                        cmd.Parameters.Add(new SqlParameter("c" + key, conditions[key]));
                    }

                    String sql = String.Format("SELECT count (*) FROM {0} WHERE {1};", table, conds);

                    cmd.Connection = conn;
                    cmd.CommandText = sql;
                    return_value = cmd.ExecuteScalar();
                }
            }
            return (int)return_value;
        }

        public static object insert(string table, Dictionary<string, object> values)
        {
            object return_value;
            using (var conn = get_open_connection())
            {
                using (var cmd = new SqlCommand())
                {
                    var cols = new StringBuilder();
                    var vals = new StringBuilder();

                    var not_null_keys = values.Keys.Where(key => values[key] != null);
                    foreach (var key in not_null_keys)
                    {
                        if (cols.Length > 0) cols.Append(",");
                        if (vals.Length > 0) vals.Append(",");

                        cols.Append(key);
                        vals.Append("@").Append(key);
                        cmd.Parameters.Add(new SqlParameter(key, values[key]));
                    }

                    var sql = String.Format("INSERT INTO {0} ({1}) VALUES ({2}); SELECT scope_identity()", table, cols, vals);

                    cmd.Connection = conn;
                    cmd.CommandText = sql;
                    return_value = cmd.ExecuteScalar();
                }
            }
            return return_value;
        }

        public static object update(string table, Dictionary<string, object> values, Dictionary<string, object> conditions)
        {
            object return_value;
            using (var conn = get_open_connection())
            {
                using (var cmd = new SqlCommand())
                {
                    StringBuilder sets = new StringBuilder();
                    StringBuilder conds = new StringBuilder();

                    foreach (var pair in values)
                    {
                        if (sets.Length > 0) sets.Append(",");
                        if (pair.Value != null)
                        {
                            sets.AppendFormat("{0} = @v{0}", pair.Key);
                            cmd.Parameters.Add(new SqlParameter("v" + pair.Key, pair.Value));
                        }
                        else
                        {
                            sets.AppendFormat("{0} = NULL", pair.Key);
                        }
                    }
                    foreach (string key in conditions.Keys)
                    {
                        if (conds.Length > 0) conds.Append(" AND ");
                        conds.AppendFormat("{0} = @c{0}", key);
                        cmd.Parameters.Add(new SqlParameter("c" + key, conditions[key]));
                    }

                    String sql = String.Format("UPDATE {0} SET {1} WHERE {2};", table, sets, conds);

                    cmd.Connection = conn;
                    cmd.CommandText = sql;
                    return_value = cmd.ExecuteScalar();
                }
            }
            return return_value;
        }

        public static object insert_or_update(string table, Dictionary<string, object> values, Dictionary<string, object> conditions)
        {
            if (count(table, conditions) > 0)
            {
                return update(table, values, conditions);
            }
            else
            {
                return insert(table, values);
            }
        }

        public static object delete(string table, Dictionary<string, object> conditions)
        {
            object return_value;
            using (var conn = get_open_connection())
            {
                using (var cmd = new SqlCommand())
                {
                    StringBuilder conds = new StringBuilder();
                    if (conditions.Count < 1) throw new ArgumentException("'conditions' cannot be empty (use 1=1 if you REALLY want to delete everything");

                    foreach (string key in conditions.Keys)
                    {
                        if (conds.Length > 0) conds.Append(" AND ");
                        conds.AppendFormat("{0} = @c{0}", key);
                        cmd.Parameters.Add(new SqlParameter("c" + key, conditions[key]));
                    }

                    String sql = String.Format("DELETE FROM {0} WHERE {1};", table, conds);

                    cmd.Connection = conn;
                    cmd.CommandText = sql;
                    return_value = cmd.ExecuteScalar();
                }
            }
            return return_value;
        }

        public static DataTableCollection get_datatables(string sql, params object[] parameters)
        {
            DataTableCollection return_value;
            using (var conn = get_open_connection())
            using (var cmd = get_command(sql, conn, parameters))
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return_value = ds.Tables;
            }
            return return_value;
        }

        public static DataTable get_datatable(string sql, params object[] parameters)
        {
            var table = new DataTable();
            using (var conn = get_open_connection())
            using (var cmd = get_command(sql, conn, parameters))
            using (var sdr = cmd.ExecuteReader())
            {
                table.Load(sdr);
            }
            return table;
        }

        public static IEnumerable<DataRow> get_datarows(string sql, params object[] parameters)
        {
            return get_datatable(sql, parameters).Rows.Cast<DataRow>();
        }

        public static DataRow get_datarow(string sql, params object[] parameters)
        {
            var dtb = get_datatable(sql, parameters);
            if (dtb.Rows.Count > 0)
            {
                return dtb.Rows[0];
            }
            return null;
        }

        public static void increment(string table, string column, string where, params object[] parameters)
        {
            validateToken(table);
            validateToken(column);

            execute(String.Format("UPDATE [{0}] SET [{1}] = COALESCE([{1}], 0) + 1 WHERE {2}", table, column, where), parameters);
        }

        public static void decrement(string table, string column, string where, params object[] parameters)
        {
            validateToken(table);
            validateToken(column);

            execute(String.Format("UPDATE [{0}] SET [{1}] = COALESCE([{1}], 0) - 1 WHERE {2}", table, column, where), parameters);
        }

        private static void validateToken(string token)
        {
            if (Regex.IsMatch(token, @"[^\w\s]"))
            {
                throw new ArgumentException("Value is invalid", "token");
            }
        }

        private static SqlConnection get_open_connection()
        {
            //  var conn = new SqlConnection(ConfigurationSettings.AppSettings["Development"] + ";Pooling=true;Connect Timeout=2;");
            var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Local"].ConnectionString + ";Pooling=true;Connect Timeout=2;User ID=" + ConfigurationManager.AppSettings["remoteDbUser"] + ";Password=" + ConfigurationManager.AppSettings["remoteDbSecret"]);
            conn.Open();

            using (var cmd = new SqlCommand())
            {
                cmd.Connection = conn;
                cmd.CommandText = "SET ARITHABORT ON";
                cmd.ExecuteScalar();
            }

            return conn;
        }

        private static SqlCommand get_command(string sql, SqlConnection conn, params object[] parameters)
        {
            var cmd = new SqlCommand
            {
                Connection = conn,
                CommandText = sql
            };

            for (int i = 0; i < parameters.Length; i++)
            {
                cmd.Parameters.Add(new SqlParameter(i.ToString(), parameters[i]));
            }
            return cmd;
        }

        public static void execute_sql(string sql)
        {
            using (var conn = get_open_connection())
            using (var cmd = get_command(sql, conn))
                cmd.ExecuteNonQuery();
        }
    }
}
