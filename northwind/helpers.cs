﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Text;

namespace northwind
{
    public static class Helpers
    {
        public static string ConvertByteArrayToBase64String(byte[] bytes)
        {
            return Convert.ToBase64String(bytes);
            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(bytes, 78, bytes.Length - 78);
                return Convert.ToBase64String(ms.ToArray());
            }
        }

        public static byte[] ConvertBase64StringToByteArray(string aString)
        {
                return Convert.FromBase64String(aString);
        }
    }
}
