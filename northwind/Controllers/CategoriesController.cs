﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using northwind.Models;
using Newtonsoft.Json;
using System.IO;
using northwind;
using System.Net.Http;
using System.Net;

namespace northwind.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        // GET: api/Categories
        [HttpGet]
        public IActionResult GetAllCategories()
        {
            try
            {
                var category_rows = DataAccess.get_datarows(
                "SELECT dbo.Categories.CategoryID, dbo.Categories.CategoryName, dbo.Categories.Description, dbo.Categories.Picture " +
                "FROM dbo.Categories;");

                List<Category> list_of_categories = new List<Category>();

                foreach (var category_row in category_rows)
                {
                    Category category = new Category()
                    {
                        CategoryId = Convert.ToInt32(category_row["CategoryID"]),
                        CategoryName = Convert.ToString(category_row["CategoryName"]),
                        Description = Convert.ToString(category_row["Description"]),
                        Picture = "data:image/bmp;base64," + Helpers.ConvertByteArrayToBase64String((byte[])category_row["Picture"])
                    };

                    list_of_categories.Add(category);
                }

                return StatusCode(200, list_of_categories);
            } catch(Exception e)
            {
                return StatusCode(500);
            }
        }

        // GET: api/Categories/GetCategoryByCategoryId?cateogryId=5
        [HttpGet("[action]")]
        public IActionResult GetCategoryByCategoryId()
        {
            int categoryId = 0;

            //  Return 400 if the categoryId variable was not passed in
            if (Request.Query["categoryId"].ToString() == "")
            {
                return StatusCode(400, "Missing parameter 'categoryId'.");
            } else
            {
                //  Return 400 if the cateogryId variable wasn't an int
                if (!Int32.TryParse(Request.Query["categoryId"], out categoryId))
                {
                    return StatusCode(400, "The categoryId parameter was not of type 'int'.");
                }
            }

            try
            {
                var category_row = DataAccess.get_datarow(
                "SELECT dbo.Categories.CategoryID, dbo.Categories.CategoryName, dbo.Categories.Description, dbo.Categories.Picture " +
                "FROM dbo.Categories " +
                "WHERE dbo.Categories.CategoryID = @0;", categoryId);

                //  Return 400 if the query yielded no results
                if(category_row == null)
                {
                    return StatusCode(400, $"Could not find any categories with an id of {categoryId}.");
                }
                else
                {
                    var category = new Category()
                    {
                        CategoryId = Convert.ToInt32(category_row["CategoryID"]),
                        CategoryName = Convert.ToString(category_row["CategoryName"]),
                        Description = Convert.ToString(category_row["Description"]),
                        Picture = "data:image/bmp;base64," + Helpers.ConvertByteArrayToBase64String((byte[])category_row["Picture"])
                    };

                    return StatusCode(200, category);
                }
            } catch(Exception e)
            {
                return StatusCode(500, "Internal Error");
            }
        }

        [HttpPost("[action]")]
        public void UpdateCategory([FromBody] Category category)
        {
            var z = category.Picture.Split(',');
            category.Picture = z[1];
            var categoryz = new Dictionary<string, object>()
            {
                { "CategoryName", category.CategoryName },
                { "Description", category.Description },
                { "Picture", Helpers.ConvertBase64StringToByteArray(category.Picture) }
            };

            var a = Helpers.ConvertBase64StringToByteArray(category.Picture);

            var conditions = new Dictionary<string, object>()
            {
                { "CategoryID", category.CategoryId }
            };

            try
            {
                DataAccess.update("dbo.Categories", categoryz, conditions);
            } catch(Exception e)
            {
                
            }
        }
        
        // POST: api/Categories
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Categories/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
