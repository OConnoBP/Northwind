﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using northwind.Models;
using Newtonsoft.Json;

namespace northwind.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        // GET: api/Products
        [HttpGet]
        public IActionResult GetAllProducts()
        {
            var product_rows = DataAccess.get_datarows(
                "SELECT dbo.Products.ProductID, dbo.Products.ProductName, dbo.Products.QuantityPerUnit, dbo.Products.UnitPrice, dbo.Products.UnitsInStock, dbo.Products.UnitsOnOrder, dbo.Products.ReorderLevel, dbo.Products.Discontinued, " +
                "dbo.Categories.CategoryID, dbo.Categories.CategoryName, " +
                "dbo.Suppliers.SupplierID, dbo.Suppliers.CompanyName " +
                "FROM dbo.Products " +
                "INNER JOIN dbo.Categories ON dbo.Products.CategoryID = dbo.Categories.CategoryID " +
                "INNER JOIN dbo.Suppliers ON dbo.Products.SupplierID = dbo.Suppliers.SupplierID " +
                "ORDER BY dbo.Products.ProductID;");

            List<Product> list_of_products = new List<Product>();

            foreach (var product_row in product_rows)
            {
                Supplier supplier = new Supplier()
                {
                    SupplierId = Convert.ToInt32(product_row["SupplierID"]),
                    CompanyName = Convert.ToString(product_row["CompanyName"])
                };

                Category category = new Category()
                {
                    CategoryId = Convert.ToInt32(product_row["CategoryID"]),
                    CategoryName = Convert.ToString(product_row["CategoryName"])
                };

                Product product = new Product()
                {
                    ProductId = Convert.ToInt32(product_row["ProductID"]),
                    ProductName = Convert.ToString(product_row["ProductName"]),
                    Supplier = supplier,
                    Category = category,
                    QuantityPerUnit = product_row["QuantityPerUnit"] == DBNull.Value ? null : Convert.ToString(product_row["QuantityPerUnit"]),
                    UnitPrice = product_row["UnitPrice"] == DBNull.Value ? 9999M : Convert.ToDecimal(product_row["UnitPrice"]),
                    UnitsInStock = product_row["UnitsInStock"] == DBNull.Value ? -1 : Convert.ToInt32(product_row["UnitsInStock"]),
                    UnitsOnOrder = product_row["UnitsOnOrder"] == DBNull.Value ? -1 : Convert.ToInt32(product_row["UnitsOnOrder"]),
                    ReorderLevel = product_row["ReorderLevel"] == DBNull.Value ? -1 : Convert.ToInt32(product_row["ReorderLevel"]),
                    Discontinued = product_row["Discontinued"] == DBNull.Value ? false : Convert.ToBoolean(product_row["Discontinued"])
                };

                list_of_products.Add(product);
            }

            return StatusCode(200, list_of_products);
        }

        // GET: api/GetProductByProductId?ProductId=5
        [HttpGet("[action]")]
        public IActionResult GetProductByProductId()
        {
            int id = Convert.ToInt32(Request.Query["productId"]);

            var product_row = DataAccess.get_datarow(
                "SELECT dbo.Products.ProductID, dbo.Products.ProductName, dbo.Products.SupplierID, dbo.Products.CategoryID, dbo.Products.QuantityPerUnit, dbo.Products.UnitPrice, dbo.Products.UnitsInStock, dbo.Products.UnitsOnOrder, dbo.Products.ReorderLevel, dbo.Products.Discontinued, " +
                "dbo.Suppliers.CompanyName, dbo.Suppliers.SupplierID, " +
                "dbo.Categories.CategoryName " +
                "FROM dbo.Products " +
                "INNER JOIN dbo.Categories ON dbo.Products.CategoryID = dbo.Categories.CategoryID " +
                "INNER JOIN dbo.Suppliers ON dbo.Products.SupplierID = dbo.Suppliers.SupplierID " +
                "WHERE dbo.Products.ProductID = @0;", id);

            Supplier supplier = new Supplier()
            {
                SupplierId = Convert.ToInt32(product_row["SupplierID"]),
                CompanyName = Convert.ToString(product_row["CompanyName"])
            };

            Category category = new Category()
            {
                CategoryName = Convert.ToString(product_row["CategoryName"])
            };

            Product product = new Product()
            {
                ProductId = Convert.ToInt32(product_row["ProductID"]),
                ProductName = Convert.ToString(product_row["ProductName"]),
                Supplier = supplier,
                Category = category,
                QuantityPerUnit = product_row["QuantityPerUnit"] == DBNull.Value ? null : Convert.ToString(product_row["QuantityPerUnit"]),
                UnitPrice = product_row["UnitPrice"] == DBNull.Value ? 9999M : Convert.ToDecimal(product_row["UnitPrice"]),
                UnitsInStock = product_row["UnitsInStock"] == DBNull.Value ? -1 : Convert.ToInt32(product_row["UnitsInStock"]),
                UnitsOnOrder = product_row["UnitsOnOrder"] == DBNull.Value ? -1 : Convert.ToInt32(product_row["UnitsOnOrder"]),
                ReorderLevel = product_row["ReorderLevel"] == DBNull.Value ? -1 : Convert.ToInt32(product_row["ReorderLevel"]),
                Discontinued = product_row["Discontinued"] == DBNull.Value ? false : Convert.ToBoolean(product_row["Discontinued"])
            };

            return StatusCode(200, product);
        }

        // GET: api/GetProductByCategoryId?CategoryId=5
        [HttpGet("[action]")]
        public IActionResult GetProductsByCategoryId()
        {
            int id = Convert.ToInt32(Request.Query["categoryId"]);

            var product_rows = DataAccess.get_datarows(
                "SELECT dbo.Products.ProductID, dbo.Products.ProductName, dbo.Products.QuantityPerUnit, dbo.Products.UnitPrice, dbo.Products.UnitsInStock, dbo.Products.UnitsOnOrder, dbo.Products.ReorderLevel, dbo.Products.Discontinued, " +
                "dbo.Categories.CategoryID, dbo.Categories.CategoryName, dbo.Categories.Description, dbo.Categories.Picture," +
                "dbo.Suppliers.SupplierID, dbo.Suppliers.CompanyName " +
                "FROM dbo.Products " +
                "INNER JOIN dbo.Categories ON dbo.Products.CategoryID = dbo.Categories.CategoryID " +
                "INNER JOIN dbo.Suppliers ON dbo.Products.SupplierID = dbo.Suppliers.SupplierID " +
                "WHERE dbo.Products.CategoryID = @0", id);

            List<Product> list_of_products = new List<Product>();

            foreach (var product_row in product_rows)
            {
                Category category = new Category()
                {
                    CategoryId = Convert.ToInt32(product_row["CategoryID"]),
                    CategoryName = Convert.ToString(product_row["CategoryName"])
                };

                Supplier supplier = new Supplier()
                {
                    SupplierId = Convert.ToInt32(product_row["SupplierID"]),
                    CompanyName = Convert.ToString(product_row["CompanyName"])
                };

                Product product = new Product()
                {
                    ProductId = Convert.ToInt32(product_row["ProductID"]),
                    ProductName = Convert.ToString(product_row["ProductName"]),
                    Supplier = supplier,
                    Category = category,
                    QuantityPerUnit = product_row["QuantityPerUnit"] == DBNull.Value ? null : Convert.ToString(product_row["QuantityPerUnit"]),
                    UnitPrice = product_row["UnitPrice"] == DBNull.Value ? 9999M : Convert.ToDecimal(product_row["UnitPrice"]),
                    UnitsInStock = product_row["UnitsInStock"] == DBNull.Value ? -1 : Convert.ToInt32(product_row["UnitsInStock"]),
                    UnitsOnOrder = product_row["UnitsOnOrder"] == DBNull.Value ? -1 : Convert.ToInt32(product_row["UnitsOnOrder"]),
                    ReorderLevel = product_row["ReorderLevel"] == DBNull.Value ? -1 : Convert.ToInt32(product_row["ReorderLevel"]),
                    Discontinued = product_row["Discontinued"] == DBNull.Value ? false : Convert.ToBoolean(product_row["Discontinued"])
                };

                list_of_products.Add(product);
            }

            return StatusCode(200, list_of_products);
        }

        [HttpGet("[action]")]
        public IActionResult GetProductsBySupplierId()
        {
            int id = Convert.ToInt32(Request.Query["supplierId"]);

            var product_rows = DataAccess.get_datarows(
                "SELECT dbo.Products.ProductID, dbo.Products.ProductName, dbo.Products.QuantityPerUnit, dbo.Products.UnitPrice, dbo.Products.UnitsInStock, dbo.Products.UnitsOnOrder, dbo.Products.ReorderLevel, dbo.Products.Discontinued, " +
                "dbo.Categories.CategoryID, dbo.Categories.CategoryName, dbo.Categories.Description, dbo.Categories.Picture," +
                "dbo.Suppliers.SupplierID, dbo.Suppliers.CompanyName " +
                "FROM dbo.Products " +
                "INNER JOIN dbo.Categories ON dbo.Products.CategoryID = dbo.Categories.CategoryID " +
                "INNER JOIN dbo.Suppliers ON dbo.Products.SupplierID = dbo.Suppliers.SupplierID " +
                "WHERE dbo.Suppliers.SupplierID = @0", id);

            List<Product> list_of_products = new List<Product>();

            foreach (var product_row in product_rows)
            {
                Category category = new Category()
                {
                    CategoryId = Convert.ToInt32(product_row["CategoryID"]),
                    CategoryName = Convert.ToString(product_row["CategoryName"])
                };

                Supplier supplier = new Supplier()
                {
                    SupplierId = Convert.ToInt32(product_row["SupplierID"]),
                    CompanyName = Convert.ToString(product_row["CompanyName"])
                };

                Product product = new Product()
                {
                    ProductId = Convert.ToInt32(product_row["ProductID"]),
                    ProductName = Convert.ToString(product_row["ProductName"]),
                    Supplier = supplier,
                    Category = category,
                    QuantityPerUnit = product_row["QuantityPerUnit"] == DBNull.Value ? null : Convert.ToString(product_row["QuantityPerUnit"]),
                    UnitPrice = product_row["UnitPrice"] == DBNull.Value ? 9999M : Convert.ToDecimal(product_row["UnitPrice"]),
                    UnitsInStock = product_row["UnitsInStock"] == DBNull.Value ? -1 : Convert.ToInt32(product_row["UnitsInStock"]),
                    UnitsOnOrder = product_row["UnitsOnOrder"] == DBNull.Value ? -1 : Convert.ToInt32(product_row["UnitsOnOrder"]),
                    ReorderLevel = product_row["ReorderLevel"] == DBNull.Value ? -1 : Convert.ToInt32(product_row["ReorderLevel"]),
                    Discontinued = product_row["Discontinued"] == DBNull.Value ? false : Convert.ToBoolean(product_row["Discontinued"])
                };

                list_of_products.Add(product);
            }

            return StatusCode(200, list_of_products);
        }

        [HttpGet("[action]")]
        public IActionResult GetProductsByOrderId()
        {
            int id = Convert.ToInt32(Request.Query["orderId"]);

            var product_rows = DataAccess.get_datarows(
                "SELECT dbo.Products.ProductID, dbo.Products.ProductName, dbo.Products.UnitPrice, dbo.Products.Discontinued, " +
                "dbo.Categories.CategoryID, dbo.Categories.CategoryName, dbo.Categories.Description, dbo.Categories.Picture," +
                "dbo.Suppliers.SupplierID, dbo.Suppliers.CompanyName " +
                "FROM dbo.Products " +
                "INNER JOIN dbo.Categories ON dbo.Products.CategoryID = dbo.Categories.CategoryID " +
                "INNER JOIN dbo.Suppliers ON dbo.Products.SupplierID = dbo.Suppliers.SupplierID " +
                "INNER JOIN dbo.[Order Details] ON dbo.Products.ProductID = dbo.[Order Details].ProductID " +
                "WHERE dbo.[Order Details].OrderID = @0", id);

            List<Product> list_of_products = new List<Product>();

            foreach (var product_row in product_rows)
            {
                Category category = new Category()
                {
                    CategoryId = Convert.ToInt32(product_row["CategoryID"]),
                    CategoryName = Convert.ToString(product_row["CategoryName"])
                };

                Supplier supplier = new Supplier()
                {
                    SupplierId = Convert.ToInt32(product_row["SupplierID"]),
                    CompanyName = Convert.ToString(product_row["CompanyName"])
                };

                Product product = new Product()
                {
                    ProductId = Convert.ToInt32(product_row["ProductID"]),
                    ProductName = Convert.ToString(product_row["ProductName"]),
                    Supplier = supplier,
                    Category = category,
                    UnitPrice = product_row["UnitPrice"] == DBNull.Value ? 9999M : Convert.ToDecimal(product_row["UnitPrice"]),
                    Discontinued = product_row["Discontinued"] == DBNull.Value ? false : Convert.ToBoolean(product_row["Discontinued"])
                };

                list_of_products.Add(product);
            }

            return StatusCode(200, list_of_products);
        }

        // POST: api/Products
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Products/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
