﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using northwind.Models;
using Newtonsoft.Json;

namespace northwind.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        // GET: api/Customers
        [HttpGet]
        public IActionResult GetAllCustomers()
        {
            try
            {
                var customer_rows = DataAccess.get_datarows(
                "SELECT dbo.Customers.CustomerId, dbo.Customers.CompanyName, dbo.Customers.ContactName, dbo.Customers.ContactTitle, dbo.Customers.Address, dbo.Customers.City, dbo.Customers.Region, dbo.Customers.PostalCode, dbo.Customers.Country, dbo.Customers.Phone, dbo.Customers.Fax, dbo.Customers.Bool " +
                "FROM dbo.Customers;");

                List<Customer> list_of_customers = new List<Customer>();

                foreach (var customer_row in customer_rows)
                {
                    var customer = new Customer()
                    {
                        CustomerId = Convert.ToString(customer_row["CustomerID"]),
                        CompanyName = Convert.ToString(customer_row["CompanyName"]),
                        ContactName = customer_row["ContactName"] == DBNull.Value ? null : Convert.ToString(customer_row["ContactName"]),
                        ContactTitle = customer_row["ContactTitle"] == DBNull.Value ? null : Convert.ToString(customer_row["ContactTitle"]),
                        Address = customer_row["Address"] == DBNull.Value ? null : Convert.ToString(customer_row["Address"]),
                        City = customer_row["City"] == DBNull.Value ? null : Convert.ToString(customer_row["City"]),
                        Region = customer_row["Region"] == DBNull.Value ? null : Convert.ToString(customer_row["Region"]),
                        PostalCode = customer_row["PostalCode"] == DBNull.Value ? null : Convert.ToString(customer_row["PostalCode"]),
                        Country = customer_row["PostalCode"] == DBNull.Value ? null : Convert.ToString(customer_row["Country"]),
                        Phone = customer_row["Phone"] == DBNull.Value ? null : Convert.ToString(customer_row["Phone"]),
                        Fax = customer_row["Fax"] == DBNull.Value ? null : Convert.ToString(customer_row["Fax"]),
                        Bool = customer_row["Bool"] == DBNull.Value ? false : Convert.ToBoolean(customer_row["Bool"])
                    };

                    list_of_customers.Add(customer);
                }

                return StatusCode(200, list_of_customers);
            } catch(Exception e)
            {
                return StatusCode(500);
            }
        }

        // GET: api/Customers/GetCustomerByCustomerId?customerId=5
        [HttpGet("[action]")]
        public IActionResult GetCustomerByCustomerId()
        {
            string id = Request.Query["customerId"].ToString();

            if(id == "")
            {
                return StatusCode(400, "Missing parameter 'customerId'.");
            }

            try
            {
                var customer_row = DataAccess.get_datarow(
                "SELECT dbo.Customers.CustomerID, dbo.Customers.CompanyName, dbo.Customers.ContactName, dbo.Customers.ContactTitle, dbo.Customers.Address, dbo.Customers.City, dbo.Customers.Region, dbo.Customers.PostalCode, dbo.Customers.Country, dbo.Customers.Phone, dbo.Customers.Fax, dbo.Customers.Bool " +
                "FROM dbo.Customers " +
                "WHERE dbo.Customers.CustomerID = @0;", id);

                if(customer_row == null)
                {
                    return StatusCode(400, $"Could not find any customers with an id of {id}.");
                }

                Customer customer = new Customer()
                {
                    CustomerId = Convert.ToString(customer_row["CustomerID"]),
                    CompanyName = Convert.ToString(customer_row["CompanyName"]),
                    ContactName = customer_row["ContactName"] == DBNull.Value ? null : Convert.ToString(customer_row["ContactName"]),
                    ContactTitle = customer_row["ContactTitle"] == DBNull.Value ? null : Convert.ToString(customer_row["ContactTitle"]),
                    Address = customer_row["Address"] == DBNull.Value ? null : Convert.ToString(customer_row["Address"]),
                    City = customer_row["City"] == DBNull.Value ? null : Convert.ToString(customer_row["City"]),
                    Region = customer_row["Region"] == DBNull.Value ? null : Convert.ToString(customer_row["Region"]),
                    PostalCode = customer_row["PostalCode"] == DBNull.Value ? null : Convert.ToString(customer_row["PostalCode"]),
                    Country = customer_row["PostalCode"] == DBNull.Value ? null : Convert.ToString(customer_row["Country"]),
                    Phone = customer_row["Phone"] == DBNull.Value ? null : Convert.ToString(customer_row["Phone"]),
                    Fax = customer_row["Phone"] == DBNull.Value ? null : Convert.ToString(customer_row["Fax"]),
                    Bool = customer_row["Bool"] == DBNull.Value ? false : Convert.ToBoolean(customer_row["Bool"])
                };

                return StatusCode(200, customer);
            } catch(Exception e)
            {
                return StatusCode(500);
            }
            /*
            var customer_row = DataAccess.get_datarow(
                "SELECT dbo.Customers.CustomerID, dbo.Customers.CompanyName, dbo.Customers.ContactName, dbo.Customers.ContactTitle, dbo.Customers.Address, dbo.Customers.City, dbo.Customers.Region, dbo.Customers.PostalCode, dbo.Customers.Country, dbo.Customers.Phone, dbo.Customers.Fax, dbo.Customers.Bool " +
                "FROM dbo.Customers " +
                "INNER JOIN dbo.Orders ON dbo.Customers.CustomerID = dbo.Orders.CustomerID " +
                "WHERE dbo.Customers.CustomerID = @0;", id);

            var order_rows = DataAccess.get_datarows(
                "SELECT dbo.Orders.OrderID, dbo.Orders.OrderDate, dbo.Orders.RequiredDate, dbo.Orders.ShippedDate, dbo.Orders.Freight, " +
                "dbo.Employees.TitleOfCourtesy, dbo.Employees.FirstName, dbo.Employees.LastName " +
                "FROM dbo.Orders " +
                "INNER JOIN dbo.Employees ON dbo.Orders.EmployeeID = dbo.Employees.EmployeeID " +
                "INNER JOIN dbo.Customers ON dbo.Orders.CustomerID = dbo.Customers.CustomerID " +
                "WHERE dbo.Customers.CustomerID = @0", id);            

            List<Order> orders = new List<Order>();

            foreach(var order in order_rows)
            {
                Employee employee = new Employee()
                {
                    TitleOfCourtesy = Convert.ToString(order["TitleOfCourtesy"]),
                    FirstName = Convert.ToString(order["FirstName"]),
                    LastName = Convert.ToString(order["LastName"])
                };

                Order orderA = new Order()
                {
                    OrderId = Convert.ToInt32(order["OrderID"]),
                    Employee = employee,
                };

                orders.Add(orderA);
            }

            Customer customer = new Customer()
            {
                CustomerId = Convert.ToString(customer_row["CustomerID"]),
                CompanyName = Convert.ToString(customer_row["CompanyName"]),
                ContactName = customer_row["ContactName"] == DBNull.Value ? null : Convert.ToString(customer_row["ContactName"]),
                ContactTitle = customer_row["ContactTitle"] == DBNull.Value ? null : Convert.ToString(customer_row["ContactTitle"]),
                Address = customer_row["Address"] == DBNull.Value ? null : Convert.ToString(customer_row["Address"]),
                City = customer_row["City"] == DBNull.Value ? null : Convert.ToString(customer_row["City"]),
                Region = customer_row["Region"] == DBNull.Value ? null : Convert.ToString(customer_row["Region"]),
                PostalCode = customer_row["PostalCode"] == DBNull.Value ? null : Convert.ToString(customer_row["PostalCode"]),
                Country = customer_row["PostalCode"] == DBNull.Value ? null : Convert.ToString(customer_row["Country"]),
                Phone = customer_row["Phone"] == DBNull.Value ? null : Convert.ToString(customer_row["Phone"]),
                Fax = customer_row["Phone"] == DBNull.Value ? null : Convert.ToString(customer_row["Fax"]),
                Bool = customer_row["Bool"] == DBNull.Value ? false : Convert.ToBoolean(customer_row["Bool"]),
                Orders = orders
            };

            return JsonConvert.SerializeObject(customer);*/
        }

        // POST: api/Customers
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Customers/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
