﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using northwind.Models;
using Newtonsoft.Json;

namespace northwind.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuppliersController : ControllerBase
    {
        // GET: api/Suppliers
        [HttpGet("[action]")]
        public IActionResult GetSuppliers()
        {
            var supplier_rows = DataAccess.get_datarows(
                "SELECT dbo.Suppliers.SupplierID, dbo.Suppliers.CompanyName, dbo.Suppliers.Address, dbo.Suppliers.City, dbo.Suppliers.Region, dbo.Suppliers.PostalCode, dbo.Suppliers.Country " +
                "FROM dbo.Suppliers;");

            List<Supplier> list_of_suppliers = new List<Supplier>();

            foreach (var supplier_row in supplier_rows)
            {
                Supplier supplier = new Supplier()
                {
                    SupplierId = Convert.ToInt32(supplier_row["SupplierID"]),
                    CompanyName = Convert.ToString(supplier_row["CompanyName"]),
                    Address = Convert.ToString(supplier_row["Address"]),
                    City = Convert.ToString(supplier_row["City"]),
                    Region = Convert.ToString(supplier_row["Region"]),
                    PostalCode = Convert.ToString(supplier_row["PostalCode"]),
                    Country = Convert.ToString(supplier_row["Country"])
                };

                list_of_suppliers.Add(supplier);
            }

            return StatusCode(200, list_of_suppliers);
        }

        // GET: api/Suppliers/5
        [HttpGet("[action]")]
        public IActionResult GetSupplierBySupplierId()
        {
            int id = Convert.ToInt32(Request.Query["supplierId"]);

            var product_rows = DataAccess.get_datarows(
                "SELECT dbo.Products.ProductID, dbo.Products.ProductName, dbo.Products.UnitPrice, dbo.Products.Discontinued, " +
                "dbo.Categories.CategoryName " +
                "FROM dbo.Products " +
                "INNER JOIN dbo.Suppliers ON dbo.Products.SupplierID = dbo.Suppliers.SupplierID " +
                "INNER JOIN dbo.Categories ON dbo.Products.CategoryID = dbo.Categories.CategoryID " +
                "WHERE dbo.Products.SupplierID = @0;", id);

            List<Product> list_of_products = new List<Product>();

            foreach (var product_row in product_rows)
            {
                Category category = new Category()
                {
                    CategoryName = Convert.ToString(product_row["CategoryName"])
                };

                Product product = new Product()
                {
                    ProductId = Convert.ToInt32(product_row["ProductID"]),
                    ProductName = Convert.ToString(product_row["ProductName"]),
                    Category = category,
                    UnitPrice = product_row["UnitPrice"] == DBNull.Value ? 9999M : Convert.ToDecimal(product_row["UnitPrice"]),
                    Discontinued = product_row["Discontinued"] == DBNull.Value ? false : Convert.ToBoolean(product_row["Discontinued"])
                };

                list_of_products.Add(product);
            }

            var supplier_row = DataAccess.get_datarow(
                "SELECT dbo.Suppliers.SupplierID, dbo.Suppliers.CompanyName, dbo.Suppliers.ContactName, dbo.Suppliers.ContactTitle, dbo.Suppliers.Address, dbo.Suppliers.City, dbo.Suppliers.Region, dbo.Suppliers.PostalCode, dbo.Suppliers.Country, dbo.Suppliers.Phone, dbo.Suppliers.Fax, dbo.Suppliers.HomePage " +
                "FROM dbo.Suppliers " +
                "WHERE dbo.Suppliers.SupplierID = @0;", id);

            Supplier supplier = new Supplier()
            {
                SupplierId = Convert.ToInt32(supplier_row["SupplierID"]),
                CompanyName = Convert.ToString(supplier_row["CompanyName"]),
                ContactName = Convert.ToString(supplier_row["ContactName"]),
                ContactTitle = Convert.ToString(supplier_row["ContactTitle"]),
                Address = Convert.ToString(supplier_row["Address"]),
                City = Convert.ToString(supplier_row["City"]),
                Region = Convert.ToString(supplier_row["Region"]),
                PostalCode = Convert.ToString(supplier_row["PostalCode"]),
                Country = Convert.ToString(supplier_row["Country"]),
                Phone = Convert.ToString(supplier_row["Phone"]),
                Fax = Convert.ToString(supplier_row["Fax"]),
                HomePage = Convert.ToString(supplier_row["HomePage"]),
                Products = list_of_products
            };

            return StatusCode(200, supplier);
        }

        // POST: api/Suppliers
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Suppliers/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
