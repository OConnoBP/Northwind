﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using northwind.Models;
using Newtonsoft.Json;

namespace northwind.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        // GET: api/Employee/GetEmployees
        [HttpGet("[action]")]
        public IActionResult GetEmployees()
        {
            var employee_rows = DataAccess.get_datarows(
                "SELECT dbo.Employees.EmployeeID, dbo.Employees.LastName, dbo.Employees.FirstName, dbo.Employees.Title, dbo.Employees.TitleOfCourtesy, dbo.Employees.BirthDate, dbo.Employees.HireDate, dbo.Employees.Address, dbo.Employees.City, dbo.Employees.Region, dbo.Employees.PostalCode, dbo.Employees.Country, dbo.Employees.HomePhone, dbo.Employees.Extension, dbo.Employees.Photo, dbo.Employees.Notes, dbo.Employees.ReportsTo, dbo.Employees.PhotoPath, " +
                "B.EmployeeID AS SupervisorID, B.TitleOfCourtesy AS SupervisorTitleOfCourtesy, B.FirstName AS SupervisorFirstName, B.LastName AS SupervisorLastName " +
                "FROM dbo.Employees " +
                "LEFT OUTER JOIN dbo.Employees B ON dbo.Employees.ReportsTo = B.EmployeeID;");

            List<Employee> list_of_employees = new List<Employee>();

            foreach (var employee_row in employee_rows)
            {
                var supervisor = new Employee()
                {
                    EmployeeId = employee_row["SupervisorID"] == DBNull.Value ? -1 : Convert.ToInt32(employee_row["SupervisorID"]),
                    TitleOfCourtesy = employee_row["SupervisorTitleOfCourtesy"] == DBNull.Value ? null : Convert.ToString(employee_row["SupervisorTitleOfCourtesy"]),
                    LastName = employee_row["SupervisorLastName"] == DBNull.Value ? null : Convert.ToString(employee_row["SupervisorLastName"]),
                    FirstName = employee_row["SupervisorFirstName"] == DBNull.Value ? null : Convert.ToString(employee_row["SupervisorFirstName"])
                };

                var employee = new Employee()
                {
                    EmployeeId = Convert.ToInt32(employee_row["EmployeeId"]),
                    LastName = Convert.ToString(employee_row["LastName"]),
                    FirstName = Convert.ToString(employee_row["FirstName"]),
                    Title = Convert.ToString(employee_row["Title"]),
                    TitleOfCourtesy = Convert.ToString(employee_row["TitleOfCourtesy"]),
                    BirthDate = Convert.ToDateTime(employee_row["BirthDate"]).ToLongDateString(),
                    HireDate = Convert.ToDateTime(employee_row["HireDate"]).ToLongDateString(),
                    Address = employee_row["Address"] == DBNull.Value ? null : Convert.ToString(employee_row["Address"]),
                    City = employee_row["City"] == DBNull.Value ? null : Convert.ToString(employee_row["City"]),
                    Region = employee_row["Region"] == DBNull.Value ? null : Convert.ToString(employee_row["Region"]),
                    PostalCode = employee_row["PostalCode"] == DBNull.Value ? null : Convert.ToString(employee_row["PostalCode"]),
                    Country = employee_row["Country"] == DBNull.Value ? null : Convert.ToString(employee_row["Country"]),
                    HomePhone = employee_row["HomePhone"] == DBNull.Value ? null : Convert.ToString(employee_row["HomePhone"]),
                    Extension = employee_row["Extension"] == DBNull.Value ? null : Convert.ToString(employee_row["Extension"]),
                    Photo = employee_row["Photo"] == DBNull.Value ? null : "data:image/bmp;base64," + Helpers.ConvertByteArrayToBase64String((byte[])employee_row["Photo"]),
                    Notes = employee_row["Notes"] == DBNull.Value ? null : Convert.ToString(employee_row["Notes"]),
                    ReportsTo = employee_row["ReportsTo"] == DBNull.Value ? -1 : Convert.ToInt32(employee_row["ReportsTo"]),
                    PhotoPath = employee_row["PhotoPath"] == DBNull.Value ? null : Convert.ToString(employee_row["PhotoPath"]),
                    Supervisor = supervisor
                };

                list_of_employees.Add(employee);
            }

            return StatusCode(200, list_of_employees);
        }

        // GET: api/Employee/5
        [HttpGet("[action]")]
        public IActionResult GetEmployeeByEmployeeId()
        {
            int id = Convert.ToInt32(Request.Query["employeeId"]);

            var employee_row = DataAccess.get_datarow(
                "SELECT dbo.Employees.EmployeeID, dbo.Employees.LastName, dbo.Employees.FirstName, dbo.Employees.Title, dbo.Employees.TitleOfCourtesy, dbo.Employees.BirthDate, dbo.Employees.HireDate, dbo.Employees.Address, dbo.Employees.City, dbo.Employees.Region, dbo.Employees.PostalCode, dbo.Employees.Country, dbo.Employees.HomePhone, dbo.Employees.Extension, dbo.Employees.Photo, dbo.Employees.Notes, dbo.Employees.ReportsTo, dbo.Employees.PhotoPath, " +
                "B.TitleOfCourtesy AS SupervisorTitleOfCourtesy, B.FirstName AS SupervisorFirstName, B.LastName AS SupervisorLastName " +
                "FROM dbo.Employees " +
                "LEFT OUTER JOIN dbo.Employees B ON dbo.Employees.ReportsTo = B.EmployeeID " +
                "WHERE dbo.Employees.EmployeeID = @0;", id);

            var supervisor = new Employee()
            {
                TitleOfCourtesy = employee_row["SupervisorTitleOfCourtesy"] == DBNull.Value ? null : Convert.ToString(employee_row["SupervisorTitleOfCourtesy"]),
                LastName = employee_row["SupervisorLastName"] == DBNull.Value ? null : Convert.ToString(employee_row["SupervisorLastName"]),
                FirstName = employee_row["SupervisorFirstName"] == DBNull.Value ? null : Convert.ToString(employee_row["SupervisorFirstName"])
            };

            var employee = new Employee()
            {
                EmployeeId = Convert.ToInt32(employee_row["EmployeeId"]),
                LastName = Convert.ToString(employee_row["LastName"]),
                FirstName = Convert.ToString(employee_row["FirstName"]),
                Title = Convert.ToString(employee_row["Title"]),
                TitleOfCourtesy = Convert.ToString(employee_row["TitleOfCourtesy"]),
                BirthDate = Convert.ToDateTime(employee_row["BirthDate"]).ToLongDateString(),
                HireDate = Convert.ToDateTime(employee_row["HireDate"]).ToLongDateString(),
                Address = employee_row["Address"] == DBNull.Value ? null : Convert.ToString(employee_row["Address"]),
                City = employee_row["City"] == DBNull.Value ? null : Convert.ToString(employee_row["City"]),
                Region = employee_row["Region"] == DBNull.Value ? null : Convert.ToString(employee_row["Region"]),
                PostalCode = employee_row["PostalCode"] == DBNull.Value ? null : Convert.ToString(employee_row["PostalCode"]),
                Country = employee_row["Country"] == DBNull.Value ? null : Convert.ToString(employee_row["Country"]),
                HomePhone = employee_row["HomePhone"] == DBNull.Value ? null : Convert.ToString(employee_row["HomePhone"]),
                Extension = employee_row["Extension"] == DBNull.Value ? null : Convert.ToString(employee_row["Extension"]),
                Photo = employee_row["Photo"] == DBNull.Value ? null : "data:image/bmp;base64," + Helpers.ConvertByteArrayToBase64String((byte[])employee_row["Photo"]),
                Notes = employee_row["Notes"] == DBNull.Value ? null : Convert.ToString(employee_row["Notes"]),
                ReportsTo = employee_row["ReportsTo"] == DBNull.Value ? -1 : Convert.ToInt32(employee_row["ReportsTo"]),
                PhotoPath = employee_row["PhotoPath"] == DBNull.Value ? null : Convert.ToString(employee_row["PhotoPath"]),
                Supervisor = supervisor
            };

            return StatusCode(200, employee);
        }

        // POST: api/Employee
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Employee/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
