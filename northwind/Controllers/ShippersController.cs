﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using northwind.Models;
using Newtonsoft.Json;

namespace northwind.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShippersController : ControllerBase
    {
        // GET: api/Shippers
        [HttpGet("[action]")]
        public IActionResult GetShippers()
        {
            var shipper_rows = DataAccess.get_datarows(
                "SELECT dbo.Shippers.ShipperID, dbo.Shippers.CompanyName, dbo.Shippers.Phone " +
                "FROM dbo.Shippers;");

            List<Shipper> list_of_shippers = new List<Shipper>();

            foreach(var shipper_row in shipper_rows)
            {
                Shipper shipper = new Shipper()
                {
                    ShipperId = Convert.ToInt32(shipper_row["ShipperID"]),
                    CompanyName = Convert.ToString(shipper_row["CompanyName"]),
                    Phone = Convert.ToString(shipper_row["Phone"])
                };

                list_of_shippers.Add(shipper);
            }

            return StatusCode(200, list_of_shippers);
        }

        // GET: api/Shippers/5
        [HttpGet("{id}", Name = "GetShipper")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Shippers
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Shippers/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
