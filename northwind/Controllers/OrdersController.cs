﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using northwind.Models;
using Newtonsoft.Json;

namespace northwind.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        // GET: api/Orders/GetOrders
        [HttpGet("[action]")]
        public IActionResult GetOrders()
        {
            var order_rows = DataAccess.get_datarows(
                "SELECT dbo.Orders.OrderID, dbo.Orders.CustomerID, dbo.Orders.EmployeeID, dbo.Orders.OrderDate, dbo.Orders.RequiredDate, dbo.Orders.ShippedDate, dbo.Orders.ShipVia, dbo.Orders.Freight, dbo.Orders.ShipName, dbo.Orders.ShipAddress, dbo.Orders.ShipCity, dbo.Orders.ShipRegion, dbo.Orders.ShipPostalCode, dbo.Orders.ShipCountry, " +
                "dbo.Employees.EmployeeID, dbo.Employees.LastName, dbo.Employees.FirstName, dbo.Employees.TitleOfCourtesy, " +
                "dbo.Customers.CustomerID, dbo.Customers.CompanyName AS CustomerName, " +
                "dbo.Shippers.ShipperID, dbo.Shippers.CompanyName AS ShipperName " +
                "FROM dbo.Orders " +
                "INNER JOIN dbo.Employees ON dbo.Orders.EmployeeID = dbo.Employees.EmployeeID " +
                "INNER JOIN dbo.Customers ON dbo.Orders.CustomerID = dbo.Customers.CustomerID " +
                "INNER JOIN dbo.Shippers ON dbo.Orders.ShipVia = dbo.Shippers.ShipperID;");

            List<Order> list_of_orders = new List<Order>();

            foreach (var order_row in order_rows)
            {
                Employee employee = new Employee()
                {
                    EmployeeId = Convert.ToInt32(order_row["EmployeeID"]),
                    TitleOfCourtesy = Convert.ToString(order_row["TitleOfCourtesy"]),
                    FirstName = Convert.ToString(order_row["FirstName"]),
                    LastName = Convert.ToString(order_row["LastName"])
                };

                Customer customer = new Customer()
                {
                    CustomerId = Convert.ToString(order_row["CustomerID"]),
                    CompanyName = Convert.ToString(order_row["CustomerName"])
                };

                Shipper shipper = new Shipper()
                {
                    ShipperId = Convert.ToInt32(order_row["ShipperID"]),
                    CompanyName = Convert.ToString(order_row["ShipperName"])
                };

                Order order = new Order()
                {
                    OrderId = Convert.ToInt32(order_row["OrderID"]),
                    Customer = customer,
                    Employee = employee,
                    OrderDate = order_row["OrderDate"] == DBNull.Value ? null : Convert.ToDateTime(order_row["OrderDate"]).ToString("MM/dd/yyyy"),
                    RequiredDate = order_row["RequiredDate"] == DBNull.Value ? null : Convert.ToDateTime(order_row["RequiredDate"]).ToString("MM/dd/yyyy"),
                    ShippedDate = order_row["ShippedDate"] == DBNull.Value ? null : Convert.ToDateTime(order_row["ShippedDate"]).ToString("MM/dd/yyyy"),
                    Shipper = shipper,
                    Freight = order_row["Freight"] == DBNull.Value ? -1M : Convert.ToDecimal(order_row["Freight"]),
                    ShipName = order_row["ShipName"] == DBNull.Value ? null : Convert.ToString(order_row["ShipName"]),
                    ShipAddress = order_row["ShipAddress"] == DBNull.Value ? null : Convert.ToString(order_row["ShipAddress"]),
                    ShipCity = order_row["ShipCity"] == DBNull.Value ? null : Convert.ToString(order_row["ShipCity"]),
                    ShipRegion = order_row["ShipRegion"] == DBNull.Value ? null : Convert.ToString(order_row["ShipRegion"]),
                    ShipPostalCode = order_row["ShipPostalCode"] == DBNull.Value ? null : Convert.ToString(order_row["ShipPostalCode"]),
                    ShipCountry = order_row["ShipCountry"] == DBNull.Value ? null : Convert.ToString(order_row["ShipCountry"])
                };

                list_of_orders.Add(order);
            }

            return StatusCode(200, list_of_orders);
        }

        [HttpGet("[action]")]
        public IActionResult GetOrdersByEmployeeId()
        {
            int id = Convert.ToInt32(Request.Query["employeeId"]);

            var order_rows = DataAccess.get_datarows(
                "SELECT dbo.Orders.OrderID, dbo.Orders.CustomerID, dbo.Orders.EmployeeID, dbo.Orders.OrderDate, dbo.Orders.RequiredDate, dbo.Orders.ShippedDate, dbo.Orders.ShipVia, dbo.Orders.Freight, dbo.Orders.ShipName, dbo.Orders.ShipAddress, dbo.Orders.ShipCity, dbo.Orders.ShipRegion, dbo.Orders.ShipPostalCode, dbo.Orders.ShipCountry, " +
                "dbo.Employees.EmployeeID, dbo.Employees.LastName, dbo.Employees.FirstName, dbo.Employees.TitleOfCourtesy, " +
                "dbo.Customers.CustomerID, dbo.Customers.CompanyName AS CustomerName, " +
                "dbo.Shippers.ShipperID, dbo.Shippers.CompanyName AS ShipperName " +
                "FROM dbo.Orders " +
                "INNER JOIN dbo.Employees ON dbo.Orders.EmployeeID = dbo.Employees.EmployeeID " +
                "INNER JOIN dbo.Customers ON dbo.Orders.CustomerID = dbo.Customers.CustomerID " +
                "INNER JOIN dbo.Shippers ON dbo.Orders.ShipVia = dbo.Shippers.ShipperID " +
                "WHERE dbo.Employees.EmployeeID = @0;", id);

            List<Order> list_of_orders = new List<Order>();

            foreach (var order_row in order_rows)
            {
                Employee employee = new Employee()
                {
                    EmployeeId = Convert.ToInt32(order_row["EmployeeID"]),
                    TitleOfCourtesy = Convert.ToString(order_row["TitleOfCourtesy"]),
                    FirstName = Convert.ToString(order_row["FirstName"]),
                    LastName = Convert.ToString(order_row["LastName"])
                };

                Customer customer = new Customer()
                {
                    CustomerId = Convert.ToString(order_row["CustomerID"]),
                    CompanyName = Convert.ToString(order_row["CustomerName"])
                };

                Shipper shipper = new Shipper()
                {
                    ShipperId = Convert.ToInt32(order_row["ShipperID"]),
                    CompanyName = Convert.ToString(order_row["ShipperName"])
                };

                Order order = new Order()
                {
                    OrderId = Convert.ToInt32(order_row["OrderID"]),
                    Customer = customer,
                    Employee = employee,
                    OrderDate = order_row["OrderDate"] == DBNull.Value ? null : Convert.ToDateTime(order_row["OrderDate"]).ToString("MM/dd/yyyy"),
                    RequiredDate = order_row["RequiredDate"] == DBNull.Value ? null : Convert.ToDateTime(order_row["RequiredDate"]).ToString("MM/dd/yyyy"),
                    ShippedDate = order_row["ShippedDate"] == DBNull.Value ? null : Convert.ToDateTime(order_row["ShippedDate"]).ToString("MM/dd/yyyy"),
                    Shipper = shipper,
                    Freight = order_row["Freight"] == DBNull.Value ? -1M : Convert.ToDecimal(order_row["Freight"]),
                    ShipName = order_row["ShipName"] == DBNull.Value ? null : Convert.ToString(order_row["ShipName"]),
                    ShipAddress = order_row["ShipAddress"] == DBNull.Value ? null : Convert.ToString(order_row["ShipAddress"]),
                    ShipCity = order_row["ShipCity"] == DBNull.Value ? null : Convert.ToString(order_row["ShipCity"]),
                    ShipRegion = order_row["ShipRegion"] == DBNull.Value ? null : Convert.ToString(order_row["ShipRegion"]),
                    ShipPostalCode = order_row["ShipPostalCode"] == DBNull.Value ? null : Convert.ToString(order_row["ShipPostalCode"]),
                    ShipCountry = order_row["ShipCountry"] == DBNull.Value ? null : Convert.ToString(order_row["ShipCountry"])
                };

                list_of_orders.Add(order);
            }

            return StatusCode(200, list_of_orders);
        }

        [HttpGet("[action]")]
        public IActionResult GetOrdersByCustomerId()
        {
            string id = Convert.ToString(Request.Query["customerId"]);

            var order_rows = DataAccess.get_datarows(
                "SELECT dbo.Orders.OrderID, dbo.Orders.CustomerID, dbo.Orders.EmployeeID, dbo.Orders.OrderDate, dbo.Orders.RequiredDate, dbo.Orders.ShippedDate, dbo.Orders.ShipVia, dbo.Orders.Freight, dbo.Orders.ShipName, dbo.Orders.ShipAddress, dbo.Orders.ShipCity, dbo.Orders.ShipRegion, dbo.Orders.ShipPostalCode, dbo.Orders.ShipCountry, " +
                "dbo.Employees.EmployeeID, dbo.Employees.LastName, dbo.Employees.FirstName, dbo.Employees.TitleOfCourtesy, " +
                "dbo.Customers.CompanyName AS CustomerName, " +
                "dbo.Shippers.ShipperID, dbo.Shippers.CompanyName AS ShipperName " +
                "FROM dbo.Orders " +
                "INNER JOIN dbo.Employees ON dbo.Orders.EmployeeID = dbo.Employees.EmployeeID " +
                "INNER JOIN dbo.Customers ON dbo.Orders.CustomerID = dbo.Customers.CustomerID " +
                "INNER JOIN dbo.Shippers ON dbo.Orders.ShipVia = dbo.Shippers.ShipperID " +
                "WHERE dbo.Customers.CustomerID = @0;", id);

            List<Order> list_of_orders = new List<Order>();

            foreach (var order_row in order_rows)
            {
                Employee employee = new Employee()
                {
                    EmployeeId = Convert.ToInt32(order_row["EmployeeID"]),
                    TitleOfCourtesy = Convert.ToString(order_row["TitleOfCourtesy"]),
                    FirstName = Convert.ToString(order_row["FirstName"]),
                    LastName = Convert.ToString(order_row["LastName"])
                };

                Customer customer = new Customer()
                {
                    CompanyName = Convert.ToString(order_row["CustomerName"])
                };

                Shipper shipper = new Shipper()
                {
                    ShipperId = Convert.ToInt32(order_row["ShipperID"]),
                    CompanyName = Convert.ToString(order_row["ShipperName"])
                };

                Order order = new Order()
                {
                    OrderId = Convert.ToInt32(order_row["OrderID"]),
                    Customer = customer,
                    Employee = employee,
                    OrderDate = order_row["OrderDate"] == DBNull.Value ? null : Convert.ToDateTime(order_row["OrderDate"]).ToString("MM/dd/yyyy"),
                    RequiredDate = order_row["RequiredDate"] == DBNull.Value ? null : Convert.ToDateTime(order_row["RequiredDate"]).ToString("MM/dd/yyyy"),
                    ShippedDate = order_row["ShippedDate"] == DBNull.Value ? null : Convert.ToDateTime(order_row["ShippedDate"]).ToString("MM/dd/yyyy"),
                    Shipper = shipper,
                    Freight = order_row["Freight"] == DBNull.Value ? -1M : Convert.ToDecimal(order_row["Freight"]),
                    ShipName = order_row["ShipName"] == DBNull.Value ? null : Convert.ToString(order_row["ShipName"]),
                    ShipAddress = order_row["ShipAddress"] == DBNull.Value ? null : Convert.ToString(order_row["ShipAddress"]),
                    ShipCity = order_row["ShipCity"] == DBNull.Value ? null : Convert.ToString(order_row["ShipCity"]),
                    ShipRegion = order_row["ShipRegion"] == DBNull.Value ? null : Convert.ToString(order_row["ShipRegion"]),
                    ShipPostalCode = order_row["ShipPostalCode"] == DBNull.Value ? null : Convert.ToString(order_row["ShipPostalCode"]),
                    ShipCountry = order_row["ShipCountry"] == DBNull.Value ? null : Convert.ToString(order_row["ShipCountry"])
                };

                list_of_orders.Add(order);
            }

            return StatusCode(200, list_of_orders);
        }

        // GET: api/Order/5
        [HttpGet("[action]")]
        public IActionResult GetOrderByOrderId()
        {
            int id = Convert.ToInt32(Request.Query["orderId"]);

            var order_row = DataAccess.get_datarow(
                "SELECT dbo.Orders.OrderID, dbo.Orders.OrderDate, dbo.Orders.RequiredDate, dbo.Orders.ShippedDate, dbo.Orders.Freight, dbo.Orders.ShipName, dbo.Orders.ShipAddress, dbo.Orders.ShipCity, dbo.Orders.ShipRegion, dbo.Orders.ShipPostalCode, dbo.Orders.ShipCountry, " +
                "dbo.Employees.LastName, dbo.Employees.FirstName, dbo.Employees.TitleOfCourtesy, " +
                "dbo.Customers.CompanyName " +
                "FROM dbo.Orders " +
                "INNER JOIN dbo.Employees ON dbo.Orders.EmployeeID = dbo.Employees.EmployeeID " +
                "INNER JOIN dbo.Customers ON dbo.Orders.CustomerID = dbo.Customers.CustomerID " +
                "WHERE dbo.Orders.OrderID = @0", id);

            Customer customer = new Customer()
            {
                CompanyName = Convert.ToString(order_row["CompanyName"])
            };

            Employee employee = new Employee()
            {
                TitleOfCourtesy = Convert.ToString(order_row["TitleOfCourtesy"]),
                FirstName = Convert.ToString(order_row["FirstName"]),
                LastName = Convert.ToString(order_row["LastName"])
            };

            var order_detail_rows = DataAccess.get_datarows(
                "SELECT dbo.[Order Details].UnitPrice, dbo.[Order Details].Quantity, dbo.[Order Details].Discount, " +
                "dbo.Products.ProductName " +
                "FROM dbo.[Order Details] " +
                "INNER JOIN dbo.Products ON dbo.[Order Details].ProductID = dbo.Products.ProductID " +
                "WHERE dbo.[Order Details].OrderID = @0;", id);

            List<OrderDetail> order_details = new List<OrderDetail>();

            foreach(var order_detail_row in order_detail_rows)
            {
                Product product = new Product()
                {
                    ProductName = Convert.ToString(order_detail_row["ProductName"])
                };

                OrderDetail order_detail = new OrderDetail()
                {
                    OrderId = id,
                    Product = product,
                    OrderDetailsUnitPrice = Convert.ToDecimal(order_detail_row["UnitPrice"]),
                    Quantity = Convert.ToInt32(order_detail_row["Quantity"]),
                    Discount = Convert.ToDouble(order_detail_row["Discount"])
                };

                order_details.Add(order_detail);
            }

            Order order = new Order()
            {
                OrderId = Convert.ToInt32(order_row["OrderID"]),
                Customer = customer,
                Employee = employee,
                OrderDate = order_row["OrderDate"] == DBNull.Value ? null : Convert.ToDateTime(order_row["OrderDate"]).ToString("MM/dd/yyyy"),
                RequiredDate = order_row["RequiredDate"] == DBNull.Value ? null : Convert.ToDateTime(order_row["RequiredDate"]).ToString("MM/dd/yyyy"),
                ShippedDate = order_row["ShippedDate"] == DBNull.Value ? null : Convert.ToDateTime(order_row["ShippedDate"]).ToString("MM/dd/yyyy"),
                //  ShipVia = Convert.ToInt32(order_row["ShipVia"]),
                Freight = order_row["Freight"] == DBNull.Value ? -1M : Convert.ToDecimal(order_row["Freight"]),
                ShipName = order_row["ShipName"] == DBNull.Value ? null : Convert.ToString(order_row["ShipName"]),
                ShipAddress = order_row["ShipAddress"] == DBNull.Value ? null : Convert.ToString(order_row["ShipAddress"]),
                ShipCity = order_row["ShipCity"] == DBNull.Value ? null : Convert.ToString(order_row["ShipCity"]),
                ShipRegion = order_row["ShipRegion"] == DBNull.Value ? null : Convert.ToString(order_row["ShipRegion"]),
                ShipPostalCode = order_row["ShipPostalCode"] == DBNull.Value ? null : Convert.ToString(order_row["ShipPostalCode"]),
                ShipCountry = order_row["ShipCountry"] == DBNull.Value ? null : Convert.ToString(order_row["ShipCountry"]),
                OrderDetails = order_details
            };

            return StatusCode(200, order);
        }

        // POST: api/Order
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Order/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
